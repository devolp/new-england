<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package New_England
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );



		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
    <section class="question_form">
	<div class="qestion_container _amin-show">
	<?php echo do_shortcode('[contact-form-7 id="158" html_name="Do_you_have_question" title="Форма Залишилися питання" ]'); ?>
	</div>
</section>
<?php

get_footer();
