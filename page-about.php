<?php

/**
 * Template Name: template for the page about us
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package New_England
 */

get_header();

get_template_part('woocommerce/global/breadcrumb');
?>
<?php
$pllang = pll_current_language();

$lang = get_field($pllang, 'option');
?>
<section class="about-us">
    <div class="container">
        <?php $builder = get_field('about_builder');
        if ($builder) : ?>
            <div class="about-us-top">
                <div class="col-left-title">
                    <h2 class="about-us-title wow fadeInUp"><?php echo $builder['title']; ?></h2>
                </div>
                <div class="col-right">
                    <p class="about-us-text wow fadeIn" data-wow-delay="0.2s"><?php echo $builder['subtitle']; ?>
                    </p>
                </div>
            </div>
            <div class="about-us-mid">
                <?php if ($builder['image_1']) : ?>
                    <div class="col-left">
                        <div class="col-left-thumbnail wow fadeInLeft">
                            <img src="<?php echo esc_url($builder['image_1']['url']); ?>" alt="<?php echo esc_attr($builder['image_1']['alt']); ?>">
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($builder['image_2']) : ?>
                    <div class="col-right">
                        <div class="col-right-thumbnail wow fadeInRight">
                            <img src="<?php echo esc_url($builder['image_2']['url']); ?>" alt="<?php echo esc_attr($builder['image_2']['alt']); ?>">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="about-us-bottom">
                <?php if ($builder['image_3']) : ?>
                    <div class="col-left">
                        <div class="col-left-image wow fadeInLeft">
                            <img src="<?php echo esc_url($builder['image_3']['url']); ?>" alt="<?php echo esc_attr($builder['image_3']['alt']); ?>">
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-right-content wow fadeInRight">
                    <?php echo $builder['main_text']; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>

<section class="advantages">
    <div class="container">
        <h2 class="advantages-main-title wow fadeInUp"><?php the_field('title_section_advantages'); ?></h2>
        <div class="advantages-slider swiper-container">
            <div class="advantages-wrapper swiper-wrapper">

                <?php
                if (have_rows('complex_advantages')) :

                    // Loop through rows.
                    while (have_rows('complex_advantages')) : the_row(); ?>

                        <div class="advantages-item swiper-slide">
                            <div class="advantages-columns">
                                <div class="advantages-col-left wow fadeInLeft">
                                    <?php $image = get_sub_field('image');
                                    if ($image) : ?>
                                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                    <?php endif; ?>
                                </div>
                                <div class="advantages-col-right">
                                    <h3 class="advantages-title wow fadeInUp"><?php the_sub_field('advantage_title'); ?></h3>
                                    <p class="advantages-text wow fadeInRight" data-wow-delay="0.3s"><?php the_sub_field('text_advantage'); ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                <?php
                    // End loop.
                    endwhile;

                endif;

                ?>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</section>

<section class="information">
    <div class="container">
        <div class="info-slider swiper-container">
            <div class="info-wrapper swiper-wrapper">

                <?php
                if (have_rows('about_us_info')) :

                    // Loop through rows.
                    while (have_rows('about_us_info')) : the_row(); ?>

                        <div class="info-item swiper-slide wow fadeIn">
                            <div class="info-image">
                                <?php $image = get_sub_field('info-image');
                                if ($image) :
                                ?>
                                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                                <?php endif; ?>
                            </div>
                            <h2 class="info-title wow fadeInUp"><?php the_sub_field('title-info'); ?></h2>
                            <?php the_sub_field('text-info'); ?>
                        </div>
                <?php
                    // End loop.
                    endwhile;

                endif;
                ?>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</section>

<section class="virtual_tour wow fadeIn">
    <div class="virtual_item">
        <h2 class="virtual_title wow fadeInUp"><?php the_field('virtual_tour_title'); ?> </h2>
        <?php ?>

        <?php
        $term_args = [
            'taxonomy' => 'pa_building',
            'hide_empty' => false,
            'orderby'       => 'name',
        ];

        $buildings = get_terms($term_args);

        if (!is_wp_error($buildings)) :
            global $wp;
            echo '<ul>';

            foreach ($buildings as $building) {

                echo '<li><a href="' . home_url() . '/building/' . $building->slug . '" class="virtualList_item ' . $building->slug . '">'
                    . $building->name;
                if ($building->count) echo ' (' . $building->count . ')';
                echo '</a></li>';
            }

            echo '</ul>';

        endif; ?>

    </div>
    <div class="virtual_map wow fadeInRight">
        <div class="virtual_map_img">
            <?php
            $map = get_field('map_with_houses');
            if ($map) : ?>
                <img src="<?php echo esc_url($map['url']); ?>" alt="<?php echo esc_attr($map['alt']); ?>">
            <?php endif; ?>
        </div>
        <?php foreach ($buildings as $building) :
            $top_distance = get_field('top_distance', $building);
            $left_distance = get_field('left_distance', $building);
        ?>
            <div class="<?php echo 'virtual_map_item ' . $building->slug; ?>" style="<?php echo 'top: ' .  $top_distance . '%;  left: ' .  $left_distance . '%;'; ?>">
                <div class="virtual_trigger">
                    <span class="triger_inside"></span>
                </div>
                <div class="virtual_drop-down_menu">
                    <a href="<?php echo home_url() . '/building/' . $building->slug; ?>" class="virtualList_item ' . $building->slug . '"><?php echo $building->name; ?></a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="about_us_flats">
    <div class="container">
        <h2 class="wow fadeInUp"><?php the_field('title_section_flats'); ?></h2>
        <div class="about-flats-slider swiper-container">
            <div class="about_us_flats_container swiper-wrapper wow fadeIn">
                <?php
                $ids = get_field('select_flats');
                $args = array(
                    'post_type' => 'product',
                    'post__in' => $ids,
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post();

                        wc_get_template_part('content', 'product-swipper');
                    endwhile;
                }
                wp_reset_postdata();
                ?>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</section>

<section class="buying">
    <div class="container">
        <h2 class="buying-title wow fadeInUp"><?php the_field('title_section_buying'); ?></h2>
        <div class="buying-list">
            <?php
            if (have_rows('how_buying')) :
                // Loop through rows.
                while (have_rows('how_buying')) : the_row(); ?>
                    <div class="buying-item">
                        <div class="buying-item-content">
                            <h3 class="buying-item-title wow fadeInLeft"><?php the_sub_field('buying-item-title'); ?></h3>
                            <p class="buying-item-text wow fadeInRight"><?php the_sub_field('buying-item-description'); ?></p>
                        </div>
                    </div>
            <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>

<section class="documents">
    <div class="container">
        <h2 class="wow fadeInUp"><?php the_field('title_section_documents'); ?></h2>
        <div class="documents-slider swiper-container wow fadeIn">
            <div class="documents-slider-wrapper swiper-wrapper">
<?php if( have_rows('documents-slider-new') ):
    while( have_rows('documents-slider-new') ) : the_row(); ?>
        <div class="documents-slider_slide swiper-slide">
            <div class="documents-slider_image">
            <?php if(get_sub_field('file')) { ?>
                <a target="_blank" href="<?php echo get_sub_field('file'); ?>">
                    <img src="<?php echo get_sub_field('image');?>" alt="file">
                </a>
            <?php } else { ?>
                <img src="<?php echo get_sub_field('image');?>" alt="file">
            <?php } ?>
            </div>
        </div>
<?php endwhile; endif; ?>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
        <div class="documents-bottom">
            <?php
            $link = get_field('download_button');
            if ($link) :
                $link_url = $link['url'];
                $link_title = $link['title'];
            ?>
                <a class="btn-about" href="<?php echo esc_url($link_url); ?>" download><?php echo esc_html($link_title); ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="asking">

    <h2 class="asking_title wow fadeInUp"><?php echo esc_attr($lang['title_faq']); ?></h2>
    <?php
    if ($lang['add_faq']) :

        // Loop through rows.
        foreach ($lang['add_faq'] as $faq) :
    ?>
            <div class="asking_item ">
                <div class="ask_arrow_container wow fadeInLeft"><span class="ask_arrow"></span></div>
                <div class="ask_text_content wow fadeInRight">
                    <h3 class="question"><?php echo $faq['title']; ?></h3>
                    <p class="answer"><?php echo $faq['description']; ?></p>
                </div>
            </div>
    <?php
        endforeach;
    endif; ?>
</section>


<?php
get_template_part('template-parts/have-any-questions-section');

get_footer();
