<?php

/**
 * The template for displaying Blog page
 */

get_header();

get_template_part('woocommerce/global/breadcrumb');
?>
<?php
$pllang = pll_current_language();

$lang = get_field($pllang, 'option');

?>
<section class="blog">
    <div class="container">
        <div class="blog-top">
            <h2 class="blog-title wow fadeInUp"><?php echo $lang['title_section_blog']; ?></h2>
            <?php
            query_posts(array('showposts' => 1, 'order' => 'ASC'));
            $ids = array();

            while (have_posts()) :
                the_post();
                $ids[] = get_the_ID();
            ?>
                <div class="blog-top-item wow fadeIn">
                    <a href="<?php the_permalink(); ?>">
                        <div class="blog-main-image">
                            <?php if (has_post_thumbnail()) :
                                the_post_thumbnail();
                            endif; ?>
                        </div>
                    </a>
                    <span class="blog-date"><?php $date_format = get_option('date_format');
                                            echo  get_the_date($date_format); ?></span>
                    <a href="<?php the_permalink(); ?>">
                        <p class="blog-text"><?php the_title(); ?></p>
                    </a>
                </div>
            <?php
            endwhile; ?>
        </div>
        <div class="blog-main wow fadeIn">
            <?php
            query_posts(array('post__not_in' => $ids, 'showposts' => 6));
            while (have_posts()) : the_post(); ?>
                <?php get_template_part('template-parts/blog-item');
                ?>
            <?php endwhile; ?>

        </div>
        <?php if ($wp_query->max_num_pages > 1) : ?>
            <script>
                var ajaxurl = '<?php echo site_url(); ?>/wp-admin/admin-ajax.php';
                var posts_vars = '<?php echo serialize($wp_query->query_vars); ?>';
                var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
            </script>
            <div class="blog-bottom">
                <a class="btn" id="loadmore"><?php echo $lang['button_text']; ?></a>
            </div>
        <?php endif; ?>
        <script type="text/javascript">
            load = '<?= $lang['button_load']; ?>';
            loadmore = '<?= $lang['button_text']; ?>';
        </script>
    </div>
</section>

<?php
get_template_part('template-parts/have-any-questions-section');

get_footer();
