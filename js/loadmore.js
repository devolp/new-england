jQuery(function ($) {
  $("#loadmore").click(function () {
    // клик на кнопку
    $(this).text(load); // меняем текст на кнопке
    // получаем нужные переменные
    var data = {
      action: "loadmore",
      query: posts_vars,
      page: current_page,
    };
    // отправляем Ajax запрос
    $.ajax({
      url: ajaxurl,
      data: data,
      type: "POST",
      success: function (data) {
        if (data) {
          $("#loadmore").text(loadmore).before(data); // добавляем новые посты
          current_page++; // записываем новый номер страницы
          if (current_page == max_pages) $("#loadmore").remove(); // если последняя страница, удаляем кнопку
        } else {
          $("#loadmore").remove(); // если посты не были получены так же удаляем кнопку
        }
      },
    });
  });

  $("#loadmore_products").click(function () {
    // клик на кнопку
    $(this).text(load); // меняем текст на кнопке
    // получаем нужные переменные
    var data = {
      action: "loadmore_products",
      query: posts_vars,
      page: current_page,
    };
    // отправляем Ajax запрос
    $.ajax({
      url: ajaxurl,
      data: data,
      type: "POST",
      success: function (data) {
        dataNew = JSON.parse(data)
        if (dataNew) {
          $(".filter_result_container").append(dataNew.product_list_block);
          $(".filter_result_container_list").append(dataNew.product_list_goris);
          $("#loadmore_products").text(loadmore); // добавляем новые посты
          current_page++; // записываем новый номер страницы
          if (current_page == max_pages) $("#loadmore_products").remove(); // если последняя страница, удаляем кнопку
        } else {
          $("#loadmore_products").remove(); // если посты не были получены так же удаляем кнопку
        }
      },
    });
  });

  $(window).resize(function() {
    if ($(window).width() >= '768'){
      $('.filter_table_button_show_col').hide();
      $('.filter_item_container_show_col').hide();
    } 
    if($(window).width() <= '768' && $('#filterTableBtn').hasClass('active')) {
      $('.filter_table_button_show_col').show();
    }
  });

  // $('#filterTableBtn').off('click');
  $('#filterTableBtn').on('click', function(e) {
    var data = {
      action: "get_table_product",
      query: posts_vars,
    };

    if ($(window).width() <= '768'){
      $('.filter_table_button_show_col').show();
    }

    $('.filter_result_container_main').animate({opacity: '0.5'});
    $('.filter_table_head').animate({opacity: '0.5'});
    
    $.ajax({
      url: ajaxurl,
      data: data,
      type: "POST",
      success: function (dataNew) {
        if (dataNew) {
          $('#filter_table_result_desctop').html('');
          $('#filter_table_result_desctop').append(dataNew);
          initEventFilter();
          initSearchTable();
          setSelectFilterTable();
          initTableSortable();
          $('.filter_result_container_main').animate({opacity: '1'});
          $('.filter_table_head').animate({opacity: '1'});
          init_filter_button_show_col();
        }
      },
    });
  });

  function initTableSortable() {
    $('.table_filter-sorting .array_block').off('click');
    $('.table_filter-sorting .array_block').on('click', function (e) {
      // console.log('click');
      var sort_column = $(this).data('column'),
        elements = $('.filter_table_result_container'),
        target = $('#filter_table_result_desctop');

      if ($(this).hasClass('sortable--desc')) {
        $('.table_filter-sorting .array_block').removeClass('sortable--asc');
        $('.table_filter-sorting .array_block').removeClass('sortable--desc');
        $(this).addClass('sortable--asc');
        elements.sort(function (a, b) {
          var an = $(a).find('.' + sort_column).text().toString(),
              bn = $(b).find('.' + sort_column).text().toString();

          if (an && bn) {
            return bn.toUpperCase().localeCompare(an.toUpperCase(), undefined, {
              numeric: true
            });
          }

          return 0;
        });
      } else {
        $('.table_filter-sorting .array_block').removeClass('sortable--desc');
        $('.table_filter-sorting .array_block').removeClass('sortable--asc');
        $(this).addClass('sortable--desc');
        elements.sort(function (a, b) {
          var an = $(a).find('.' + sort_column).text().toString(),
              bn = $(b).find('.' + sort_column).text().toString();

          if (an && bn) {
            return an.toUpperCase().localeCompare(bn.toUpperCase(), undefined, {
              numeric: true
            });
          }

          return 0; C
        });
      }
      elements.detach().appendTo(target);
    });
  }

  function initTableFilter() {
    $('#filter_table_result_desctop .filter_table_result_container').addClass('active-product-line');
    $('.table_filter_item').each(function(i, elem){
      if ($(elem).hasClass('table_filter_item_select')) {
        var value = $(elem).children("option:selected").text();
        if (value != all_option) {
          var col = $(elem).data('table_col');
          tableFilterSelect(col, value);
        }
      } else {
        var value = $(elem).val();
        if (value != '') {
          var col = $(elem).data('table_col');
          tableFilter(col, value);
        }
      }
    });
    setSelectFilterTable();
  }

  function setSelectFilterTable() {
    $('.table_filter_item_select').each(function(i, elemEr){
      var list_option = [];
      var html_option_str = '<option value="' + all_option + '">' + all_option + '</option>';
      var value = $(elemEr).children("option:selected").text();
      var col = $(elemEr).data('table_col');

      $('#filter_table_result_desctop .filter_table_result_container.active-product-line ' + col).each(function(i, elemEl) {
        if ( list_option.indexOf( $(elemEl).text() ) == -1 && $(elemEl).text() != '' ) {
          list_option.push($(elemEl).text());
        }
      });

      list_option.forEach(function(currentValue){
        html_option_str += '<option value="' + currentValue + '">' + currentValue + '</option>';
      });

      $(elemEr).html('');
      $(elemEr).html(html_option_str);

      if ( list_option.indexOf( value ) != -1) {
        $(elemEr).val(value);
      } else {
        $(elemEr).val(all_option);
      }
    });
  }

  function initEventFilter() {
    $('.table_filter_item').not('.table_filter_item_select').off('keyup');
    $('.table_filter_item').not('.table_filter_item_select').on('keyup', function () {
      initTableFilter();
    });

    $('.table_filter_item.table_filter_item_select').off('change');
    $('.table_filter_item.table_filter_item_select').on('change', function () {
      initTableFilter();
    });
  }

  function tableFilter(col, value) {
    if (col.indexOf('fulls') > -1) {
      $('#filter_table_result_desctop .filter_table_result_container.active-product-line')
        .filter(function() { 
          $(this)
            .toggleClass('active-product-line', $(this)
              .find(col)
              .text()
              .toLowerCase() == value.toLowerCase()
            );
      });
    } else if (col.indexOf('firsts') > -1) {
      $('#filter_table_result_desctop .filter_table_result_container.active-product-line')
        .filter(function() { 
          $(this)
            .toggleClass('active-product-line', $(this)
            .find(col)
            .text()
            .toLowerCase()
            .indexOf(value.toLowerCase()) == 0
          );
      });
    } else {
      $('#filter_table_result_desctop .filter_table_result_container.active-product-line')
        .filter(function() { 
          $(this)
            .toggleClass('active-product-line', $(this)
              .find(col)
              .text()
              .toLowerCase()
              .indexOf(value.toLowerCase()) > -1
            );
      });
    }
  }

  function tableFilterSelect(col, value) {
    $('#filter_table_result_desctop .filter_table_result_container.active-product-line')
      .filter(function() { 
          $(this)
              .toggleClass('active-product-line', $(this)
                  .find(col)
                  .text()
                  .toLowerCase() == value.toLowerCase()
              );
    });
  }

  function initSearchTable() {
    $('#table_search').off('keyup');
    $('#table_search').on('keyup', function() {
      var value = $(this).val().toLowerCase();
      $('#filter_table_result_desctop .filter_table_result_container').removeClass('active-product-line');
      if (value != '') {
        $('#filter_table_result_desctop .filter_table_result_container')
            .filter(function() { 
                $(this)
                    .toggleClass('active-product-line',$(this)
                        .text()
                        .toLowerCase()
                        .indexOf(value) > -1
                    );
        });
      } else {
        $('#filter_table_result_desctop .filter_table_result_container').addClass('active-product-line');
      }
      $('.table_filter_item').val('');
      setSelectFilterTable();
    });
    $('#table_search').off('search');
    $('#table_search').on('search', function() {
      $('#filter_table_result_desctop .filter_table_result_container').addClass('active-product-line');
      $('.table_filter_item').val('');
      setSelectFilterTable();
    });
  }


  function createCookie(name, value, days) {
    var expires;
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toGMTString();
    } else {
      expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
  }

  function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ')
        c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0)
        return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return false;
  }

  function init_filter_button_show_col() {
    $('.filter-show-btn').each(function(i, item, arr) {
      var cookies =  $(item).data('cookies');
      if (readCookie(cookies) == 'hide') {
        $(item).prop('checked', false);
        $($(item).data('table_col')).hide();
        $($(item).data('filter-item')).closest('.filter_item__wrapper').hide();
        $($(item).data('filter-item')).removeClass('table_filter_item');
      }
    });

    $('.filter-show-btn').off('click');
    $('.filter-show-btn').on('click', function() {
      if ($(this).is(':checked')) {
        createCookie($(this).data('cookies'), 'show', 0.00001);
        $($(this).data('table_col')).show();
        $($(this).data('filter-item')).closest('.filter_item__wrapper').show();
        $($(this).data('filter-item')).addClass('table_filter_item');
      } else {
        createCookie($(this).data('cookies'), 'hide', 100);
        $($(this).data('table_col')).hide();
        $($(this).data('filter-item')).closest('.filter_item__wrapper').hide();
        $($(this).data('filter-item')).removeClass('table_filter_item');
      }
      initTableFilter(); 
    });
  }
});
