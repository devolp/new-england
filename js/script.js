$(document).ready(function () {
  $('.print_now').on('click', function (){
    $('.popap_close').click();
    setTimeout(function () { window.print(); }, 500);
  })
  new Swiper(".documents-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },

    slidesPerView: 3,

    spaceBetween: 60,

    speed: 500,

    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      576: {
        spaceBetween: 30,
        slidesPerView: 3,
      },
      768: {
        spaceBetween: 30,
        slidesPerView: 3,
      },
      992: {
        spaceBetween: 60,
        slidesPerView: 3,
      },
    },
  });

  new Swiper(".info-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },

    slidesPerView: 3,

    spaceBetween: 60,

    speed: 500,

    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      576: {
        spaceBetween: 30,
        slidesPerView: 2,
      },
      768: {
        spaceBetween: 30,
        slidesPerView: 3,
      },
      992: {
        spaceBetween: 60,
        slidesPerView: 3,
      },
    },
  });

  new Swiper(".advantages-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },

    slidesPerView: 1,

    speed: 500,

    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      576: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 1,
      },
      992: {
        slidesPerView: 1,
      },
    },
  });

  new Swiper(".about-flats-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },

    slidesPerView: 3,

    spaceBetween: 60,

    speed: 500,

    breakpoints: {
      320: {
        slidesPerView: 1,
      },

      576: {
        slidesPerView: 2,
      },

      768: {
        slidesPerView: 2,
        spaceBetween: 60,
      },

      992: {
        slidesPerView: 3,
      },
    },
  });


  $('.slider-for-products').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button id="back" type="button" class="photo_galery_button  back"><span></span></button>',
    nextArrow: '<button id="forward" type="button" class="photo_galery_button  forward"><span></span></button>',
    fade: true,
    asNavFor: '.slider-nav-products'
  });
  $('.slider-nav-products').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for-products',
    arrows: false,
    dots: false,
    centerMode: false,
    centerPadding: "0",
    focusOnSelect: true
  });

  


  $('.contact_as').click ( function() {
    tittleProduct =$('.contact_as').attr('data-title-product');
    buildName =$('.contact_as').attr('data-build-name');
    parking =$('.contact_as').attr('data-parking');
    pCheck=$('input.tab_button').prop('checked');

    if (pCheck === true) {
      $('#parking-cf7').val('Есть');
    }
    $('#title-product-cf7').val(tittleProduct);
    $('#build-name-cf7').val(buildName);
    console.log(pCheck);
    

  });


});


