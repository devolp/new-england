$(document).ready(function () {

  // wow
    setTimeout(function(){
      new WOW({mobile:false}).init();
    }, 2000);
  // wow end

  // Preloader

  window.onload = function () {
    document.body.classList.add("loaded_hiding");
    window.setTimeout(function () {
      document.body.classList.add("loaded");

      document.body.classList.remove("loaded_hiding");
    }, 200);
  };

  $(document).on('premmerce-filter-updated', function() {
    console.log('ajaxComplete');
    $('.filter__select').select2('destroy');
    $('.filter__select').select2({
      searchInputPlaceholder: search_name
    });
  });

  $(function () {
    $("#progress").width(50 + Math.random() * 50 + "%");
    $(window).on("load", function () {
      $("#progress").width("101%").delay(100).fadeOut(400);
    });
  });

  $('#jsSearchPopupSubmitBtn').on('click', function(e){
    e.preventDefault();
    var dataForm = $('#jsSearchForm').serializeArray();
    var cleanDataForm = dataForm.filter(function(elem) {
      return elem.value != 0;
    });
    console.log(cleanDataForm);
    var paramsStr = "";
    cleanDataForm.forEach(function(item, i, arr) {
      paramsStr = paramsStr + item.name + "=" + item.value+"&"
    });
    if (cleanDataForm.length != 0) {
      // console.log($('#jsSearchForm').attr('action') + '?' + paramsStr);
      document.location.href = $('#jsSearchForm').attr('action') + '?' + paramsStr;
    } else {
      return false;
    }
  });

  // Preloader_end

  $(
      ".widget_premmerce_filter_filter_widget + .widget_premmerce_filter_filter_widget"
  ).remove();

  $(".main_container").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 12000,
    speed: 1200,
    responsive: [
      {
        breakpoint: 375,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
          dots: false,
          arrows: false,
        },
      },
    ],
  });

  $(".aware_slider_container").slick({
    infinite: true,
    slidesToScroll: 1,
    slidesToShow: 4,
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 12000,
    speed: 1200,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 3,
          dots: false,
        },
      },
      {
        breakpoint: 680,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 2,
          dots: false,
        },
      },
      {
        breakpoint: 450,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
          dots: false,
        },
      },
    ],
  });

  $(".review_container").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: false,
    dots: true,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
          arrows: true,
        },
      },
    ],
  });

  $(".benefits_container_slider").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
          dots: false,
        },
      },
    ],
  });

  // catalog_chechbox desctop_slider

  $(".real_estate_result_container_slider_all").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".real_estate_result_container_slider_flat_one_room").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".real_estate_result_container_slider_flat_two_room").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".real_estate_result_container_slider_flat_three_room").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  // only_best_chechbox desctop_slider

  $(".only_best_container.slider.all").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".only_best_container.slider.discounts").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".only_best_container.slider.preferably").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".only_best_container.slider.shares").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".filter_table_slider").slick({
    infinite: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 765,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1.5,
        },
      },
    ],
  });

  $(".card_slider_container").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 12000,
    speed: 1200,
  });

  $(".parking_container_slider").slick({
    infinite: true,
    fade: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    arrows: true,
    dots: false,
  });

  $(".asking_item").on("click", function () {
    $(this).children(".ask_text_content").find(".answer").slideToggle();
    $(this).find(".ask_arrow").toggleClass("active");
  });

  $(".header_mobile_button").on("click", function () {
    $(this).parent().toggleClass("mobile");
    $(this).toggleClass("active");
    $("body").toggleClass("no_scroll");
  });

  $(".header_menu").on("click", function () {
    $(".header_mobile_button").removeClass("active");
    $(".header_container").removeClass("mobile");
    $("body").removeClass("no_scroll");
  });

  $(".langButton").click(function () {
    event.preventDefault();
    $(this).parent().find(".disabled").toggleClass("active");
    $(".langContainer").toggleClass("clicked");
  });

  $(".link_shares_container").on("click", function () {
    event.preventDefault();
    $(this).addClass("active");
    $(".sharelist").show();
  });

  $(".link_shares_container.active").click(function () {
    event.preventDefault();
    $(".sharelist").css("display", "none");
    $(this).removeClass("active");
  });

  // $('.header_submenu_button').click(function(){
  //   $(this).parent().parent().find(".header_submenu_container").slideToggle();
  //   $(this).toggleClass('active');
  // });

  // catalog_chechbox desctop
  const checkSliderHeight = () => {
    const sliders = $(".slick-initialized .slick-list.draggable");
    if(sliders.length > 0){
      for (let index = 0; index < sliders.length; index++) {
        if( sliders[index].offsetHeight === 0){
          $(sliders[index]).closest('.slick-initialized').addClass('emptyHeight');
        }
        else{
          $(sliders[index]).closest('.slick-initialized').removeClass('emptyHeight');
        }
      }
    }
  };
  $(".real_estate_tab").click(function () {
    $(".real_estate_tab").removeClass("active");
    $(this).toggleClass("active");
  });

  $(".real_estate_tab.all").click(function () {
    $(".real_estate_result_item").addClass("active");
    $(".real_estate_result_container_slider_all").addClass("active");
    $(".real_estate_result_container_slider_flat_1_room").removeClass("active");
    $(".real_estate_result_container_slider_flat_2_room").removeClass("active");
    $(".real_estate_result_container_slider_flat_3_room ").removeClass(
        "active"
    );
    $(".real_estate_container .real_estate_result_container").slick('refresh');
    checkSliderHeight();
  });

  $(".real_estate_tab.one").click(function () {
    $(".real_estate_result_item").removeClass("active");
    $(".real_estate_result_item.flet_1_room").addClass("active");
    $(".real_estate_result_container_slider_flat_1_room").addClass("active");
    $(".real_estate_result_container_slider_all").removeClass("active");
    $(".real_estate_result_container_slider_flat_2_room").removeClass("active");
    $(".real_estate_result_container_slider_flat_3_room ").removeClass(
        "active"
    );
    $(".real_estate_container .real_estate_result_container").slick('refresh');
    checkSliderHeight();

  });

  $(".real_estate_tab.two").click(function () {
    $(".real_estate_result_item").removeClass("active");
    $(".real_estate_result_item.flet_2_room").addClass("active");
    $(".real_estate_result_container_slider_flat_2_room").addClass("active");
    $(".real_estate_result_container_slider_all").removeClass("active");
    $(".real_estate_result_container_slider_flat_1_room").removeClass("active");
    $(".real_estate_result_container_slider_flat_3_room ").removeClass(
        "active"
    );
    $(".real_estate_container .real_estate_result_container").slick('refresh');
    checkSliderHeight();

  });

  $(".real_estate_tab.three").click(function () {
    $(".real_estate_result_item").removeClass("active");
    $(".real_estate_result_item.flet_3_room").addClass("active");
    $(".real_estate_result_container_slider_flat_3_room").addClass("active");
    $(".real_estate_result_container_slider_all").removeClass("active");
    $(".real_estate_result_container_slider_flat_1_room").removeClass("active");
    $(".real_estate_result_container_slider_flat_2_room ").removeClass(
        "active"
    );
    $(".real_estate_container .real_estate_result_container").slick('refresh');
    checkSliderHeight();
  });

  // only_best_chechbox desctop

  $(".real_estate_tab.best").click(function () {
    $(this).removeClass("active");
    $(this).toggleClass("active");
  });

  $(".real_estate_tab.best.all").click(function () {
    $(".real_estate_result_item.best").addClass("active");
    $(".real_estate_result_container_slider_all").addClass("active");
    $(".only_best_container.slider.all").addClass("active");
    $(".only_best_container.slider.preferably").removeClass("active");
    $(".only_best_container.slider.discounts").removeClass("active");
    $(".only_best_container.slider.shares").removeClass("active");
    $(".real_estate_catalog.only_best .real_estate_result_container").slick('refresh');
    checkSliderHeight();
  });

  $(".real_estate_tab.best.only").click(function () {
    $(".real_estate_result_item.best").removeClass("active");
    $(".real_estate_result_item.best.only").addClass("active");
    $(".only_best_container.slider.preferably").addClass("active");
    $(".only_best_container.slider.all").removeClass("active");
    $(".only_best_container.slider.discounts").removeClass("active");
    $(".only_best_container.slider.shares").removeClass("active");
    $(".real_estate_catalog.only_best .real_estate_result_container").slick('refresh');
    checkSliderHeight();
  });

  $(".real_estate_tab.best.discounts").click(function () {
    $(".real_estate_result_item.best").removeClass("active");
    $(".real_estate_result_item.best.discount").addClass("active");
    $(".only_best_container.slider.discounts").addClass("active");
    $(".only_best_container.slider.preferably").removeClass("active");
    $(".only_best_container.slider.all").removeClass("active");
    $(".only_best_container.slider.shares").removeClass("active");
    $(".real_estate_catalog.only_best .real_estate_result_container").slick('refresh');
    checkSliderHeight();
  });

  $(".real_estate_tab.best.shares").click(function () {
    $(".real_estate_result_item.best").removeClass("active");
    $(".real_estate_result_item.best.shares").addClass("active");
    $(".only_best_container.slider.shares").addClass("active");
    $(".only_best_container.slider.preferably").removeClass("active");
    $(".only_best_container.slider.discounts").removeClass("active");
    $(".only_best_container.slider.all").removeClass("active");
    $(".real_estate_catalog.only_best .real_estate_result_container").slick('refresh');
    checkSliderHeight();
  });

  //catalog_choise_prise

  $(".prise_button").click(function () {
    $(".choise_prise").toggleClass("active");
    $(".prise_button").toggleClass("active");
    $(".choise_prise").find(".disabled").toggleClass("active");
    $(".choise_prise").find("li").removeClass("red");
  });

  $(".choise_prise")
      .find("li")
      .click(function () {
        $(".choise_prise").find("li").removeClass("active");
        $(".choise_prise").find("li").addClass("disabled");
        $(".choise_prise").removeClass("active");
        $(this).addClass("active");
        $(this).removeClass("disabled");
        if (!$(".choise_prise").hasClass("active")) {
          $(this).addClass("red");
        }
        $(".prise_button").removeClass("active");
      });

  //filter

  $(".filter_tab_button.first").click(function () {
    event.preventDefault();
    $(".filter_tab_button").removeClass("active");
    $(this).toggleClass("active");
    $(".filter").addClass("active");
    $(".filter_hide_show").fadeOut();
    $(".filter_table_button").removeClass("active");
    $(".filter_item_container.first").fadeOut();
    $(".filter_mobile_and_table_head").fadeIn();
    $(".tab_container").fadeOut();
    $(".collapse_filter").fadeOut();
    $(".filter_result_container_table.tab_container").fadeIn();
    $(".show_more").fadeOut();
  });

  $(".filter_tab_button.second").click(function () {
    event.preventDefault();
    $(".filter_tab_button").removeClass("active");
    $(".filter_table_button").removeClass("active");
    $(this).toggleClass("active");
    $(".filter").removeClass("active");
    $(".filter_hide_show").fadeOut();
    $(".show_more").fadeIn();
    $(".filter_item_container.first").fadeOut();
    $('.filter_table_button_show_col').hide();
    if (innerWidth > 570) {
      $(".filter_item_container.first").fadeIn();
      $(".collapse_filter").fadeIn();
      $(".filter_mobile_and_table_head").fadeOut();
    }
    $(".tab_container").fadeOut();
    $(".list").fadeIn(500);
  });

  $(".filter_tab_button.last").click(function () {
    event.preventDefault();
    $(".filter_tab_button").removeClass("active");
    $(".filter_table_button").removeClass("active");
    $(this).toggleClass("active");
    $(".filter").removeClass("active");
    $(".filter_hide_show").fadeOut();
    $(".show_more").fadeIn();
    $(".filter_item_container.first").fadeOut();
    $('.filter_table_button_show_col').hide();
    $(".filter_mobile_and_table_head").fadeIn();
    if (innerWidth > 570) {
      $(".filter_item_container.first").fadeIn();
      $(".collapse_filter").fadeIn();
    }
    $(".tab_container").fadeOut();
    $(".block").fadeIn();
  });



  $(".collapse_filter").click(function (e) {
    if(lang == 'en-US') {
      var collapse_filter = 'expand filter';
      var collapse_filter_0 = 'collapse filter';
    }
    if(lang == 'ru-RU') {
      var collapse_filter = 'раскрыть фильтр';
      var collapse_filter_0 = 'свернуть фильтр';
    }
    if(lang == 'uk') {
      var collapse_filter = 'розгорнути фільтр';
      var collapse_filter_0 = 'згорнути фільтр';
    }
    e.preventDefault();

    $(this).toggleClass("active");
    if ($(this).hasClass("active")) {
      $(this).text(collapse_filter_0);
    } else {
      $(this).text(collapse_filter);
    }
    $(".widget_premmerce_filter_filter_widget").toggleClass('filter_hide_show_active');
  });

  //Slick slider for block Каталог товаров mobile version
  if (innerWidth < 680) {
    $("#real_estate_catalog .real_estate_result_container").slick({
      infinite: false,
      slidesToScroll: 1,
      slidesToShow: 1,
      arrows: true,
      dots: false,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 680,
          settings: {
            slidesToScroll: 1,
            slidesToShow: 1,
          },
        },
      ],
    });

    $("#real_estate_only_best .real_estate_result_container").slick({
      infinite: false,
      slidesToScroll: 1,
      slidesToShow: 1,
      arrows: true,
      dots: false,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 680,
          settings: {
            slidesToScroll: 1,
            slidesToShow: 1,
          },
        },
      ],
    });
  }

  $(".filter_table_button").click(function (event) {
    event.preventDefault();
    $(this).toggleClass("active");
    $(".filter_item_container.first").slideToggle();
    // $('.filter_hide_show').slideToggle();
    $(".collapse_filter").toggle();
  });

  $(".filter_table_button_show_col").click(function (event) {
    event.preventDefault();
    $(this).toggleClass("active");
    // $('.filter_item_container.first').hide();
    $(".filter_item_container_show_col").slideToggle();
  });

  $(".filter__header").click(function () {
    if (innerWidth < 570) {
      event.preventDefault();
      $(this).find(".filter_arrow").toggleClass("active");
      $(this).parent().find(".filter__inner.mobile").slideToggle();
    }
  });

  //Pop-aps
  $(".search_main_screen").click(function (e) {
    e.preventDefault();
    $(".overlay").addClass("active");
    $(".popap.search").fadeIn();
    $("body").addClass("no_scroll");
  });

  $(".contact_as").click(function (e) {
    e.preventDefault();
    $(".overlay").addClass("active");
    $(".popap.contact").fadeIn();
    $("body").addClass("no_scroll");
  });

  // $(".inspect").click(function (e) {
  //   e.preventDefault();
  //   $(".overlay").addClass("active");
  //   $("body").addClass("no_scroll");
  //   $(".modal_window#inspect-apartment").fadeIn();
  // });

  $(".link_print_container").click(function (e) {
    e.preventDefault();
    $(".overlay").addClass("active");
    $(".popap.print").fadeIn();
    $("body").addClass("no_scroll");
  });

  $(".overlay, .popap_close").click(function () {
    $(".overlay").removeClass("active");
    $(".popap.search").fadeOut();
    $(".popap.contact").fadeOut();
    $(".modal_window#inspect-apartment").fadeOut();
    $(".popap.print").fadeOut();
    $("#thanks").fadeOut();
    $("body").removeClass("no_scroll");
  });

  document.addEventListener(
      "wpcf7mailsent",
      function (event) {
        $(".modal_window#inspect-apartment").fadeOut();
        $(".popap.contact").fadeOut();
        $(".modal_window#thanks").fadeIn();
        $(".overlay").addClass("active");
        $("body").addClass("no_scroll");
      },
      false
  );

  $(".show_more").click(function (e) {
    e.preventDefault();
    $(".filter_result_item.two").toggleClass("active");
    $(".filter_result_item.two").toggle();
    if ($(".filter_result_item.two").hasClass("active")) {
      $(".result_list_item.two").removeClass("active");
    } else {
      $(".result_list_item.two").addClass("active");
      $(".filter_result_item.two").hide();
    }
  });

  /* Virtual tour */
  $(".virtualList_item").hover(
      function () {
        var classList = $(this).attr("class").split(/\s+/);
        city = classList[1];
        $(".virtual_map_item." + city)
            .find(".virtual_drop-down_menu")
            .addClass("active");
      },
      function () {
        $(".virtual_map_item." + city)
            .find(".virtual_drop-down_menu")
            .removeClass("active");
      }
  );

  //popup-video
  $("a.video_button").click(function (e) {
    e.preventDefault();
    $(".popap-video").animate(300, function () {
      $("body").addClass("popup-active-overflow");
      $(this).addClass("popup-active").animate({ opacity: 1 }, 300);
    });

    var iframe_src = $(".popap-video input.video_src").val();
    console.log(iframe_src);
    var iframe = $(".popap-video.popup-active iframe");
    iframe.attr("src", iframe_src);
    iframe.attr("id", "yt_video");

    if ($(window).width() <= 768) {
      youTubeFullscreen(iframe);
    }
  });
  $(".popap-video .popup-close").on("click", function () {
    $(".popap-video").removeClass("popup-active");
    $(".popap-video iframe").attr("src", "");
    $("body").removeClass("popup-active-overflow");
  });
  $(".popup-active-overflow .video_overlay").on("click", function () {
    $(".popap-video").removeClass("popup-active");
    $("body").removeClass("popup-active-overflow");
    $(".popap-video iframe").attr("src", "");
    console.log("click");
  });
  $(".video_overlay").on("click", function () {
    console.log("click");
    $(".popap-video iframe").attr("src", "");
    $("body").removeClass("popup-active-overflow");
    $(".popap-video.popup-active").removeClass("popup-active");
  });
  //popup-close
  $(".popup-close")
      .click(function () {
        $(".popup-block").animate({ opacity: 0 }, 300, function () {
          $(this).removeClass("popup-active");
          $("body").removeClass("popup-active-overflow");
          $(".popup-block .popup-video-content").empty();
        });
      })
      .children()
      .click(function (e) {
        e.stopPropagation();
      });

  $(".photo_disabled").click(function () {
    $(this).toggleClass("scale");
  });

  $(".photo_disabled.one").click(function () {
    $(".big").removeClass("first");
    $(".big.one").addClass("first");
  });

  $(".photo_disabled.two").click(function () {
    $(".big").removeClass("first");
    $(".big.two").addClass("first");
  });

  $(".photo_disabled.three").click(function () {
    $(".big").removeClass("first");
    $(".big.three").addClass("first");
  });

  $(".photo_disabled.four").click(function () {
    $(".big").removeClass("first");
    $(".big.four").addClass("first");
  });

  $(".photo_disabled.five").click(function () {
    $(".big").removeClass("first");
    $(".big.five").addClass("first");
  });

  //Photo_galery_counter
  var i = 0;

  $("#forward").click(function () {
    $(".photo_disabled").removeClass("first");
    document.getElementsByClassName("big")[i].classList.remove("first");
    if (i == 5) {
      i = 0;
    } else {
      i++;
    }
    document.getElementsByClassName("big")[i].classList.add("first");
  });

  $("#back").click(function () {
    $(".photo_disabled").removeClass("first");
    document.getElementsByClassName("big")[i].classList.remove("first");
    if (i == 0) {
      i = 5;
    } else {
      i--;
    }
    document.getElementsByClassName("big")[i].classList.add("first");
  });

  $('.selectable').select2({
    minimumResultsForSearch: -1
  });

  (function($) {

    var Defaults = $.fn.select2.amd.require('select2/defaults');

    $.extend(Defaults.defaults, {
      searchInputPlaceholder: ''
    });

    var SearchDropdown = $.fn.select2.amd.require('select2/dropdown/search');

    var _renderSearchDropdown = SearchDropdown.prototype.render;

    SearchDropdown.prototype.render = function(decorated) {

      // invoke parent method
      var $rendered = _renderSearchDropdown.apply(this, Array.prototype.slice.apply(arguments));

      this.$search.attr('placeholder', this.options.get('searchInputPlaceholder'));

      return $rendered;
    };

  })(window.jQuery);



  function sel() {
    if($('.filter').length) {
      $('.filter input[name="max_price"]').attr('min', 9999999);
      $('.filter input[name="max_price"]').attr('max', 9999999);

      $('.filter input[name="min_price"]').attr('min', 9999999);
      $('.filter input[name="min_price"]').attr('max', 9999999);

      // var currency_val = parseInt($('#current_currency').data('currency'));
      // var min_price = parseInt($('.filter input[name="min_price"]').val());
      // var max_price = parseInt($('.filter input[name="max_price"]').val());
      //
      // $('.filter input[name="min_price"]').val(min_price * currency_val);
      // $('.filter input[name="max_price"]').val(max_price * currency_val);
    }
    var lang = $('html').attr('lang');
    if(lang == 'en-US') {
      var search_name = 'Search...';
    }
    if(lang == 'ru-RU') {
      var search_name = 'Поиск...';
    }
    if(lang == 'uk') {
      var search_name = 'Пошук...';
    }
    $('.filter select > option:contains("(0)")').each(function (){
      $(this).attr('disabled', true);
    })
    $('.filter__select').select2({
      searchInputPlaceholder: search_name
    });

    //$(".filter input[type=checkbox]").removeAttr("disabled");
  }

  $('.filter').on('input', 'input[type="number"]', function(){
    if (this.value.length > 7){
      this.value = this.value.slice(0, this.maxLength);
    }
  });

  $('body').on('change','input[type=checkbox]', function (){
    setTimeout(sel, 2000);
    $(".filter .filter__inner input[type=checkbox]").attr("disabled","disabled");
  });
  sel();

  setInterval(sel, 5000);

  $('.woocommerce-ordering').on('change', function (){
    $(this).submit();
  });

  var lang = $('html').attr('lang');

  // $( document ).ajaxComplete(function() {
  //   $('.filter__select').select2('destroy');
  //   $('.filter__select').select2({
  //     searchInputPlaceholder: search_name
  //   });
  // });

  if ($(window).width() <= 767) {
    $(".filter__item--type-checkbox .filter__header").click(function () {
      event.preventDefault();
      $(this).toggleClass("active");
      $(this).next().find(".filter__properties-list").slideToggle();
    });
  }
  $('#formpdf + .print_link_item').on('click',function (){
    $('.print .sharelist-popup').toggle();
    return false;
  });

  $('.share_link_item').on('click',function (){
    $('.crumbs_content .sharelist-popup').toggle();
    return false;
  });

  $('#save_pdf').click(function() {
    html2canvas(document.querySelector("#pdfhtml"),{
      quality: 1,
      scale: 1.0,
      dpi: 500,
    }).then(canvas => {
      $('#pdfhtml2').html('');
      $('#pdfhtml2').append(canvas);
      var pdf_title = $('title').text();
      var imgData = canvas.toDataURL(
          'image/png');
      var doc = new jsPDF('p', 'mm');
      doc.addImage(imgData, 'PNG', 10, 10);
      doc.save(pdf_title+'.pdf');
    });
  });
  setInterval(function () {$(".filter_tax_price input[name=min_price],.filter_tax_price input[off-name=min_price],.filter_tax_price input[name=max_price],.filter_tax_price input[off-name=max_price]").removeAttr("disabled");}, 500);

  function AddReadMore() {
    //This limit you can set after how much characters you want to show Read More.
    var carLmt = 163;
    // Text to show when text is collapsed
    var readmore = "";
    if($('html[lang="uk"]').length) {
      var readmore = 'Показати більше';
    }
    if($('html[lang="ru"]').length) {
      var readmore = 'Показать больше';
    }
    if($('html[lang="en"]').length) {
      var readmore = 'Show more';
    }
    var readMoreTxt = readmore;
    // Text to show when text is expanded
    var readLessTxt = " ";


    //Traverse all selectors with this class and manupulate HTML part to show Read More
    $(".addReadMore").each(function() {
      if ($(this).find(".firstSec").length)
        return;

      // var allstr = $(this).text();
      // if (allstr.length > carLmt) {
      //   var firstSet = allstr.substring(0, carLmt);
      //   var secdHalf = allstr.substring(carLmt, allstr.length);
      //   var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><span class='readMore'  title='Click to Show More'>" + readMoreTxt + "</span><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
      //   $(this).html(strtoadd);
      // }

    });
    $('.newreadmore > span + span').append("<span class='readMore'  title=''>" + readMoreTxt + "</span>");
    //$('.newreadmore > span + span').append("<span class='readLess'  title=''>" + readLessTxt + "</span>");

    //Read More and Read Less Click Event binding
    $(document).on("click", ".readMore,.readLess", function() {
      $(this).closest(".newreadmore").toggleClass("showlesscontent showmorecontent");
      $(this).hide();

    });
  }
  $(function() {
    //Calling function after Page Load
    AddReadMore();
  });

});

function numberWithSpaces(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
function addparking() {
  var curr = $('#current_currency > div > div > div > div:nth-child(1) > div > div').data('sign');
  var price = parseInt($('#prise_in').data('total'));
  var parking =  parseInt($('#prise_in').data('parking'));

  if($('#product_checkbox:checked').length) {
    var price_w_p = numberWithSpaces(price + parking);
    $('#prise_in, .prise_in.grivna-print #prise_in').html(price_w_p + ' ' + curr);
  } else {
    $('#prise_in, .prise_in.grivna-print #prise_in').html(numberWithSpaces(price) + ' ' + curr);
  }
  if($('#product_checkbox:checked').length) {
    $('#product_checkbox_2').attr('checked', true);
  } else {
    $('#product_checkbox_2').attr('checked', false);
  }
}

function AddReadMore2() {
  var carLmt = 163;
  var readmore = "";
  if($('html[lang="uk"]').length) {
    var readmore = '... Показати більше';
  }
  if($('html[lang="ru"]').length) {
    var readmore = '... Показать больше';
  }
  if($('html[lang="en"]').length) {
    var readmore = '... Show more';
  }
  var readMoreTxt = readmore;
  // Text to show when text is expanded
  var readLessTxt = " ";

  //Traverse all selectors with this class and manupulate HTML part to show Read More
  $(".addReadMore").each(function() {
    if ($(this).find(".firstSec").length)
      return;

    var allstr = $(this).text();
    if (allstr.length > carLmt) {
      var firstSet = allstr.substring(0, carLmt);
      var secdHalf = allstr.substring(carLmt, allstr.length);
      var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><span class='readMore'  title='Click to Show More'>" + readMoreTxt + "</span><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
      $(this).html(strtoadd);
    }

  });
  //Read More and Read Less Click Event binding
  $(document).on("click", ".readMore,.readLess", function() {
    $(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
  });
}
$(function() {
  //Calling function after Page Load
  AddReadMore2();
});