<?php

/**
 * Template Name: Home Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package New_England
 */

get_header();
?>
	<?php 
	$pllang = pll_current_language();
	$lang = get_field( $pllang, 'option' );
?>
    <section class="main_screen">
   

<?php if( have_rows('home_slider') ): ?>
    <?php while( have_rows('home_slider') ): the_row(); 

$slider = get_sub_field('add_slide');
if( $slider ): ?>
    <div class="main_container">
        <?php foreach( $slider as $slide ): ?>
            <div class="main_item">
                <?php if($slide['add_slide_image'] != null && $slide['add_slide_video'] == null) { ?>
                <img src="<?php echo esc_url($slide['add_slide_image']); ?>" alt="Slider_photo">
                <?php }
                if($slide['add_slide_video'] != null && $slide['add_slide_image'] != null) { ?>
                    <a class="play_video" data-fancybox href="<?php echo esc_url($slide['add_slide_video']); ?>">
                        <img src="<?php echo esc_url($slide['add_slide_image']); ?>" alt="Slider_photo111">
                    </a>
                <?php } ?>
            </div>
        <?php endforeach; ?>
        </div>
<?php endif; ?>

<?php endwhile; ?>
<?php endif; ?>


        <div class="about_us">
        <?php $home_title = get_field('home_title_after_slider');
if( $home_title ): ?>
            <h1 class="main_screen_title"><?php echo esc_attr( $home_title['title_h1'] ); ?></h1>
            <a href="<?php echo esc_attr( $home_title['url_button'] ); ?>" class="search_main_screen"><?php echo esc_attr( $home_title['text_button'] ); ?></a>
            <?php endif; ?>
            <?php $home_about = get_field('home_about');
if( $home_about ): ?>
            <div class="about_complex">
            <div class="abount_complex_Item_first">
                <h3 class="complex_item_title wow fadeInUp"><?php echo esc_attr( $home_about['title'] ); ?></h3>
                <p class="complex_Item_text wow fadeIn" data-wow-delay="0.3s"><?php echo esc_attr( $home_about['adress'] ); ?></p>
            </div>
            <div class="abount_complex_Item_last wow fadeInRight">
                <p class="complex_Item_last_text"><?php echo esc_attr( $home_about['description'] ); ?></p>
            </div>
            </div>
            <?php endif; ?>
        </div>

    </section>     
    <section id="real_estate_catalog" class="real_estate_catalog">
      <div class="real_estate_container">
          <!-- Filter -->
            <div class="real_estate_title_container">
                <h3 class="real_estate_title wow fadeInUp"><?php echo pll_e( 'Каталог квартир' ); ?></h3>
                <div class="real_estate_tab_container">
                    <div class="real_estate_tab all active wow fadeInRight">
                        <span class="tab_button all"></span>
                        <p><?php echo pll_e('Всі'); ?></p>
                    </div>
                    <div class="real_estate_tab one wow fadeInRight" data-wow-delay="0.2s">
                        <span class="tab_button one"></span>
                        <p><?php echo pll_e( '1-кімнатні' ); ?></p>
                    </div>
                    <div class="real_estate_tab two wow fadeInRight" data-wow-delay="0.4s">
                        <span class="tab_button two"></span>
                        <p><?php echo pll_e( '2-кімнатні'  ); ?></p>
                    </div>
                    <div class="real_estate_tab three wow fadeInRight" data-wow-delay="0.6s">
                        <span class="tab_button three"></span>
                        <p><?php echo pll_e( '3-кімнатні',  ); ?></p>
                    </div>
                </div>
            </div>

            <div class="real_estate_result_container">

            <?php
   $args_product_cat = array(
    'post_type'      => 'product',
    'posts_per_page' => 2,
    'orderby' => 'rand',
    'product_cat' => "kvartira",

);

$product_cat = new WP_Query( $args_product_cat );

while ( $product_cat->have_posts() ) : $product_cat->the_post();
global $product; ?>
                    <div class="real_estate_result_item flet_<?php echo $product->get_attribute( 'pa_kimnat' ); ?>_room active">
                        <div class="result_head">
                            <p><?php echo get_the_title(); ?>   |   <?php echo $product->get_attribute( 'pa_floor' ); ?> <?php echo wc_attribute_label( 'pa_floor' ); ?></p>
                            <p class="size"><?php echo $product->get_attribute( 'pa_polscha' ); ?> <?php echo __( 'м²', 'new-england' ); ?></p>
                        </div>
                        <a href="<?php echo get_permalink(); ?>" class="real_estate_img_container">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="one_room_flat">
                        </a>
                        <a href="<?php echo get_permalink(); ?>" class="result_footer">
                            
                                <div>
                                    <p class="flat_name"><?php echo wc_attribute_label( 'pa_building' ); ?> “<?php echo $product->get_attribute( 'pa_building' ); ?>”</p>
                                    <p class="flet_prise"><?php echo $product->get_price_html(); ?>/<?php echo __( 'м²', 'new-england' ); ?></p>
                                </div>
                                <p  class="result_item_button"><span class="arrow"></span></p>
                            
                        </a>
                    </div>
 
                    <?php  endwhile; ?>
<?php   wp_reset_postdata(); ?>

            </div>
						<div class="white_line_container">
							<a href="<?php echo $lang['catalog_link']; ?>" class="in_catalog wow flipInX"><?php echo pll_e( 'перейти в каталог' ); ?></a>
						</div>
        </div>
    </section>
    <section class="benefits">
    <?php $features = get_field('home_features'); ?>
        <h2 class="benefits_title_mobile"><?php echo esc_attr( $features['title'] ); ?></h2>

        <?php if( have_rows('home_features') ): ?>
    <?php 
    
    while( have_rows('home_features') ): the_row(); 
    $i = 1;
    if( have_rows('add_features') ): 
        while( have_rows('add_features') ): the_row(); 
        // Get sub field values.
        $foto = get_sub_field('foto');
        $name = get_sub_field('name');
        $description = get_sub_field('description');

        ?>


        <? if($i == '1'){ ?>
            <div class="benefits_item first">
            <h2 class="benefits_title wow fadeInUp"><?php echo esc_attr( $features['title'] ); ?></h2>
            <div class="benefits_item_content ">
                <div class="benefits_item_photo first wow fadeIn">
                <?php if( $foto ) { ?>
                    <?php foreach( $foto as $slide ): ?>
                        <img src="<?php echo esc_url($slide['url']); ?>" alt="benefits_photo">
        <?php endforeach; ?>

                    <?php } ?>
                </div>
                <div class="benefits_text_container wow fadeInLeft">
                    <h4><?php echo esc_attr( $name ); ?></h4>
                    <p><?php echo esc_attr( $description ); ?></p>
                </div>
            </div>
        </div>
      
   <?php } else { ?>
    <div class="benefits_item">
            <div class="benefits_item_photo wow fadeIn">
            <?php if( $foto ) { ?>
                    <?php foreach( $foto as $slide ): ?>
                        <img class="images_<?php echo count($foto); ?>" src="<?php echo esc_url($slide['url']); ?>" alt="benefits_photo">
        <?php endforeach; ?>

                    <?php } ?>
            </div>
            <div class="benefits_text_container wow fadeInLeft">
                <h4><?php echo esc_attr( $name ); ?></h4>
                <p><?php echo esc_attr( $description ); ?></p>
            </div>
        </div>
    <?php } ?>
           

    <?php 
$i++;
endwhile; 
endif;
endwhile; 
 endif; ?>

        <div class="benefits_container_slider">

        <?php if( have_rows('home_features') ): ?>
    <?php 
    
    while( have_rows('home_features') ): the_row(); 
    $i = 1;
    if( have_rows('add_features') ): 
        while( have_rows('add_features') ): the_row(); 
        // Get sub field values.
        $foto = get_sub_field('foto');
        $name = get_sub_field('name');
        $description = get_sub_field('description');

        ?>
        <? if($i == '1'){ ?>
            <div class="benefits_item first">
                <div class="benefits_item_content">
                    <div class="benefits_item_photo first">
                    <?php if( $foto ) { ?>
                    <?php foreach( $foto as $slide ): ?>
                        <img src="<?php echo esc_url($slide['url']); ?>" alt="benefits_photo">
        <?php endforeach; ?>

                    <?php } ?>
                    </div>
                    <div class="benefits_text_container">
                    <h4><?php echo esc_attr( $name ); ?></h4>
                    <p><?php echo esc_attr( $description ); ?></p>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="benefits_item">
                <div class="benefits_item_photo wow fadeIn">
                <?php if( $foto ) { ?>
                    <?php foreach( $foto as $slide ): ?>
                        <img src="<?php echo esc_url($slide['url']); ?>" alt="benefits_photo">
        <?php endforeach; ?>

                    <?php } ?>
                </div>
                <div class="benefits_text_container wow fadeInLeft">
                <h4><?php echo esc_attr( $name ); ?></h4>
                    <p><?php echo esc_attr( $description ); ?></p>
                </div>
            </div>
            <?php } ?>

            <?php 
$i++;
endwhile; 
endif;
endwhile; 
 endif; ?>

               </div>
			<div class="grey_line_container">
        <a href="<?php echo esc_attr( $features['url_button'] ); ?>" class="benefits_button wow flipInX"><?php echo esc_attr( $features['name_button'] ); ?></a>
			</div>
    </section>
    <section  id="real_estate_only_best" class="real_estate_catalog only_best">

        <div class="real_estate_container">
           
            <div class="real_estate_title_container">
                  <h3 class="real_estate_title wow fadeInUp"><?php echo pll_e('Тільки найкраще'); ?></h3>
                  <div class="real_estate_tab_container">
                      <div class="real_estate_tab best all active wow fadeInRight">
                          <span class="tab_button_best all"></span>
                          <p><?php echo pll_e('Всі'); ?></p>
                      </div>
                      <div class="real_estate_tab best only wow fadeInRight" data-wow-delay="0.2s">
                          <span class="tab_button_best only"></span>
                          <p><?php echo pll_e('Тільки найкраще'); ?></p>
                      </div>
                      <div class="real_estate_tab best discounts wow fadeInRight" data-wow-delay="0.4s">
                          <span class="tab_button_best discounts"></span>
                          <p><?php echo pll_e('Знижки на нерухомість'); ?></p>
                      </div>
                      <div class="real_estate_tab best shares wow fadeInRight" data-wow-delay="0.6s">
                          <span class="tab_button_best shares"></span>
                          <p><?php echo pll_e('Акції'); ?></p>
                      </div>
                  </div>
            </div>

            <div class="real_estate_result_container">
            <?php
   $args_only_best = array(
    'post_type'      => 'product',
    'posts_per_page' => 6,
    'product_tag'=> 'shares,only,discount',
    'orderby' => 'rand',

);

$only_best = new WP_Query( $args_only_best );

while ( $only_best->have_posts() ) : $only_best->the_post();
global $product; ?>
<?php //echo wp_get_post_terms( get_the_id(), 'product_tag', array( 'fields' => 'slugs' ) ); ?>
<?php $tags = get_the_terms( get_the_ID(), 'product_tag' ); 
if( is_array( $tags ) ){
	foreach( $tags as $tag ){
        $slug_best = $tag->slug;
    }
}
?>

                    <div class="real_estate_result_item best <?php echo $slug_best;?> active wow fadeIn">
                          <div class="result_head">
                              <p><?php echo get_the_title(); ?>   |   <?php echo $product->get_attribute( 'pa_floor' ); ?> <?php echo wc_attribute_label( 'pa_floor' ); ?></p>
                              <p class="size"><?php echo $product->get_attribute( 'pa_polscha' ); ?> <?php echo __( 'м²', 'new-england' ); ?></p>
                          </div>
                          <a href="<?php echo get_permalink(); ?>" class="real_estate_img_container">
                              <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="one_room_flat">
</a>
                            <a href="<?php echo esc_url(get_the_permalink()) ?>" class="result_footer">
                              <div>
                                  <p class="flat_name"><?php echo wc_attribute_label( 'pa_building' ); ?> “<?php echo $product->get_attribute( 'pa_building' ); ?>”</p>
                                  <p class="flet_prise"><?php echo $product->get_price_html(); ?>/<?php echo __( 'м²', 'new-england' ); ?></p>
                              </div>
                              <p  class="result_item_button"><span class="arrow"></span></p>
                            </a>
                    </div>
                    <?php  endwhile; ?>
<?php   wp_reset_postdata(); ?>

            </div>

        </div>
    </section>
    <section class="be_aware">
    <?php $home_news = get_field('home_news'); ?>
        <div class="be_aware_head">
            <h2 class="aware_title wow fadeInUp"><?php echo esc_attr( $home_news['title'] ); ?></h2>
            <a href="<?php echo esc_attr( $home_news['url_button'] ); ?>" class="all_news wow flipInX"><?php echo esc_attr( $home_news['title_button'] ); ?></a>
        </div>
        <?php


$news = get_field('select_news');
if( $news ): ?>
        <div class="aware_slider_container wow fadeIn">
        <?php foreach( $news as $post ): ?>
            <a href="<?php echo get_permalink($post->ID); ?>" class="aware_item">
                <div class="aware_img_container">
                    <?php echo get_the_post_thumbnail($post->ID); ?>
                </div>
                <p class="data"><?php echo get_the_date(); ?></p>
                <p class="aware_item_text"><?php echo get_the_title($post->ID); ?></p>
            </a>

            <?php endforeach;
            wp_reset_postdata();
            ?>
        </div>
        <?php endif; ?>

    </section>
    <section class="review">
    <?php $reviews = get_field('home_reviews'); ?>
        <h2 class="review_title wow fadeInUp"><?php echo esc_attr( $reviews['title_reviews'] ); ?></h2>
        <div class="review_container desktop_slider">
        <?php if( have_rows('home_reviews') ): ?>
					<?php
					while( have_rows('home_reviews') ): the_row();
					$i = 1;
					if( have_rows('add_reviews') ): 
							while( have_rows('add_reviews') ): the_row();
							// Get sub field values.
							$ava = get_sub_field('foto');
							$name = get_sub_field('name');
							$description = get_sub_field('review');
							?>
						<? if($i % 2 != 0){ ?>
								<div class="review_item_content">
						<?php }?>
                <div class="review_item">
                    <div class="reviev_user_ava wow fadeInRight">
                        <img src="<?php echo esc_url( $ava ); ?>" alt="user_avatar">
                    </div>
                    <div class="review_item_text">
                        <h5 class="user_name wow fadeInUp"><?php echo esc_attr( $name ); ?></h5>
                        <p class="user_text_review wow fadeIn" data-wow-delay="0.3s"><?php echo esc_attr( $description ); ?></p>
                    </div>
                </div>

						<?php if($i % 2 == 0) { ?>
							</div>
						<?php } ?>
					<?php
						$i++;
						endwhile;
						endif;
						endwhile;
						endif; ?>
				</div>
				<div class="review_container mobile_slider">

        <?php if( have_rows('home_reviews') ): ?>
					<?php

					while( have_rows('home_reviews') ): the_row();
					$i = 1;
					if( have_rows('add_reviews') ): 
							while( have_rows('add_reviews') ): the_row();
							// Get sub field values.
							$ava = get_sub_field('foto');
							$name = get_sub_field('name');
							$description = get_sub_field('review');
							?>
							<div class="review_item_content">
                <div class="review_item">
                    <div class="reviev_user_ava wow fadeInRight">
                        <img src="<?php echo esc_url( $ava ); ?>" alt="user_avatar">
                    </div>
                    <div class="review_item_text">
                        <h5 class="user_name wow fadeInUp"><?php echo esc_attr( $name ); ?></h5>
                        <p class="user_text_review wow fadeIn" data-wow-delay="0.3s"><?php echo esc_attr( $description ); ?></p>
                    </div>
                </div>
							</div>
					<?php
						$i++;
						endwhile;
						endif;
						endwhile;
						endif; ?>

				</div>

    </section>

    <section class="asking">
    <?php $faq = get_field('home_section_faq'); ?>
    <h2 class="asking_title wow fadeInUp"><?php echo esc_attr( $faq['title_faq'] ); ?></h2>
    <?php
    if ($lang['add_faq']) :

        // Loop through rows.
        foreach ($lang['add_faq'] as $faq) : 
    ?>
            <div class="asking_item">
                <div class="ask_arrow_container wow fadeInDown"><span class="ask_arrow"></span></div>
                <div class="ask_text_content wow fadeInRight">
                    <h3 class="question"><?php echo $faq['title']; ?></h3>
                    <p class="answer"><?php echo $faq['description']; ?></p>
                </div>
            </div>
    <?php
        endforeach;
    endif; ?>
    </section>

<?php

get_template_part('template-parts/have-any-questions-section');

get_footer();
