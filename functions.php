<?php

/**
 * New England functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package New_England
 */
if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.1');
}

if (!function_exists('new_england_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function new_england_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on New England, use a find and replace
		 * to change 'new_england' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('new_england', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		add_theme_support('widgets');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__('Primary', 'new_england'),
				'footer_menu_1' => esc_html__('Footer Menu 1', 'new_england'),
				'footer_menu_2' => esc_html__('Footer Menu 2', 'new_england'),
			)
		);





		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'new_england_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'new_england_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function new_england_content_width()
{
	$GLOBALS['content_width'] = apply_filters('new_england_content_width', 640);
}
add_action('after_setup_theme', 'new_england_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function new_england_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'new_england'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'new_england'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'new_england_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function new_england_scripts()
{
	wp_enqueue_style('new_england-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('new_england-style', 'rtl', 'replace');

	wp_enqueue_style('swiper-style', get_template_directory_uri() . '/css/swiper-bundle.min.css', array(), _S_VERSION);
	wp_enqueue_style('slick', get_template_directory_uri() . '/css/slick-theme.css', array(), _S_VERSION);
	wp_enqueue_style('slick-theme', get_template_directory_uri() . '/css/slick.css', array(), _S_VERSION);
	wp_enqueue_style('fonts', get_template_directory_uri() . '/css/fonts.css', array(), _S_VERSION);
	wp_enqueue_style('style-main', get_template_directory_uri() . '/css/style.css', array(), _S_VERSION);
	wp_enqueue_style('brand-css', get_template_directory_uri() . '/css/my-css.css', array(), _S_VERSION);
    wp_enqueue_style('select2-style', get_template_directory_uri() . '/css/select2.min.css', array(), _S_VERSION);
	wp_enqueue_style('fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css', array(), _S_VERSION);
	wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css', array(), _S_VERSION);


     if ( is_product() ) {
	     wp_enqueue_style('product-print', get_template_directory_uri() . '/css/print.css', array(), _S_VERSION, 'print');
     }
	
	//wp_deregister_script( 'jquery' );
	wp_enqueue_script('jquery-3.5.1', get_template_directory_uri() . '/js/jquery-3.5.1.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('script-js', get_template_directory_uri() . '/js/script.js', array(), _S_VERSION, true);
	wp_enqueue_script('swiper-bundle', get_template_directory_uri() . '/js/swiper-bundle.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('apps-js', get_template_directory_uri() . '/js/apps.js', array(), _S_VERSION, true);
	wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('new_england-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);
	wp_enqueue_script('loadmore', get_stylesheet_directory_uri() . '/js/loadmore.js', array('jquery'), _S_VERSION, true);
	wp_enqueue_script('masked-input', get_template_directory_uri() . '/js/jquery.maskedinput.min.js', array('jquery'));
    wp_enqueue_script('select2', get_template_directory_uri() . '/js/select2.min.js', array(), _S_VERSION, true);

	wp_enqueue_script('jspdf', 'https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('html2canvas', 'https://html2canvas.hertzen.com/dist/html2canvas.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.js', array(), _S_VERSION, true);



	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'new_england_scripts');

/* 
 * check which template is loaded 
*/
function meks_which_template_is_loaded()
{
	if (is_super_admin()) {
		global $template;
		//print_r($template);
	}
}

add_filter('acf/location/rule_values/page_type', function ($choices) {
	$choices['woo_shop_page'] = 'WooCommerce Shop Page';
	return $choices;
});

add_filter('acf/location/rule_match/page_type', function ($match, $rule, $options) {
	if ($rule['value'] == 'woo_shop_page' && isset($options['post_id'])) {
		if ($rule['operator'] == '==')
			$match = ($options['post_id'] == wc_get_page_id('shop'));
		if ($rule['operator'] == '!=')
			$match = ($options['post_id'] != wc_get_page_id('shop'));
	}
	return $match;
}, 10, 3);

add_action('wp_footer', 'meks_which_template_is_loaded');
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if (class_exists('WooCommerce')) {
	require get_template_directory() . '/inc/woocommerce.php';
}

add_filter('woocommerce_breadcrumb_defaults', 'new_england_woocommerce_breadcrumb_defaults');

function new_england_woocommerce_breadcrumb_defaults($defaults)
{
	$defaults['home'] = '';
	return $defaults;
}

register_sidebar(array(
	'name' => __('Колонка фильтров'),
	'id' => 'filters',
	'before_widget' => '<div class="widget clearfix %2$s" >',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title-wrap"><h5 class="widget-title">',
	'after_title' => '</h5></div>',
	'description' => __('Колонка для фільтрів', 'new-england')
));
register_sidebar(array(
	'name' => __('Активні фильтри'),
	'id' => 'active_filters',
	'before_widget' => '<div class="widget clearfix %2$s" >',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title-wrap"><h5 class="widget-title">',
	'after_title' => '</h5></div>',
	'description' => __('Активні фильтри', 'new-england')
));
register_sidebar(array(
	'name' => __('Пошук нерухомості'),
	'id' => 'popup_filters',
	'before_widget' => '<div class="%2$s" >',
	'after_widget' => '</div>',
	'before_title' => '',
	'after_title' => '',
	'description' => __('Пошук нерухомості', 'new-england')
));

class Footer_Walker_Nav_Menu extends Walker_Nav_Menu
{
	/*
	 * Позволяет перезаписать <ul class="sub-menu">
	 */
	function start_lvl(&$output, $depth = 0, $args = NULL)
	{
		// для WordPress 5.3+
		// function start_lvl( &$output, $depth = 0, $args = NULL ) {
		/*
		 * $depth – уровень вложенности, например 2,3 и т д
		 */
		$output .= '<ul class="menu_sublist">';
	}
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output
	 * @param object $item Объект элемента меню, подробнее ниже.
	 * @param int $depth Уровень вложенности элемента меню.
	 * @param object $args Параметры функции wp_nav_menu
	 */
	function start_el(&$output, $item, $depth = 0, $args = NULL, $id = 0)
	{
		// для WordPress 5.3+
		// function start_el( &$output, $item, $depth = 0, $args = NULL, $id = 0 ) {
		global $wp_query;
		/*
		 * Некоторые из параметров объекта $item
		 * ID - ID самого элемента меню, а не объекта на который он ссылается
		 * menu_item_parent - ID родительского элемента меню
		 * classes - массив классов элемента меню
		 * post_date - дата добавления
		 * post_modified - дата последнего изменения
		 * post_author - ID пользователя, добавившего этот элемент меню
		 * title - заголовок элемента меню
		 * url - ссылка
		 * attr_title - HTML-атрибут title ссылки
		 * xfn - атрибут rel
		 * target - атрибут target
		 * current - равен 1, если является текущим элементом
		 * current_item_ancestor - равен 1, если текущим (открытым на сайте) является вложенный элемент данного
		 * current_item_parent - равен 1, если текущим (открытым на сайте) является родительский элемент данного
		 * menu_order - порядок в меню
		 * object_id - ID объекта меню
		 * type - тип объекта меню (таксономия, пост, произвольно)
		 * object - какая это таксономия / какой тип поста (page /category / post_tag и т д)
		 * type_label - название данного типа с локализацией (Рубрика, Страница)
		 * post_parent - ID родительского поста / категории
		 * post_title - заголовок, который был у поста, когда он был добавлен в меню
		 * post_name - ярлык, который был у поста при его добавлении в меню
		 */
		$indent = ($depth) ? str_repeat("\t", $depth) : '';

		/*
		 * Генерируем строку с CSS-классами элемента меню
		 */
		$class_names = $value = '';
		$classes = empty($item->classes) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		// функция join превращает массив в строку
		$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
		$class_names = ' class="' . esc_attr($class_names) . '"';

		/*
		 * Генерируем ID элемента
		 */
		$id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
		$id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

		/*
		 * Генерируем элемент меню
		 */
		$output .= $indent . '<li' . $id . $value . $class_names . '>';

		// атрибуты элемента, title="", rel="", target="" и href=""
		$attributes  = !empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) . '"' : '';
		$attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
		$attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
		$attributes .= !empty($item->url)        ? ' href="'   . esc_attr($item->url) . '"' : '';

		// ссылка и околоссылочный текст
		$item_output = $args->before;
		if (!$item->url) {
			$item_output .= '<h5 class="footer_menu_title">';
			$item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
			$item_output .= '</h5>';
		} else {
			$item_output .= '<a' . $attributes . ' class="footer_menu_item" >';
			$item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
			$item_output .= '</a>';
		}
		$item_output .= $args->after;

		$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
	}
}

remove_filter('the_content', 'wpautop');


/* 
* Load more posts
*/

function loadmore_get_posts()
{

	$args = unserialize(stripslashes($_POST['query']));
	$args['paged'] = $_POST['page'] + 1; // next page
	$args['post_status'] = 'publish';
	$args['showposts'] = 6;

	query_posts($args);
	// if have posts
	if (have_posts()) :
		while (have_posts()) : the_post();
			the_post(); ?>
		<?php get_template_part('template-parts/blog-item');

		endwhile;
	endif;
	die();
}

function loadmore_get_products()
{
	$args = unserialize(stripslashes($_POST['query']));
	$args['paged'] = $_POST['page'] + 1; // next page
	$args['post_status'] = 'publish';
	$args['showposts'] = 6;

	query_posts($args);

	ob_start();
		woocommerce_product_loop_start();
		if (wc_get_loop_prop('total')) :
			while (have_posts()) :
				the_post();
				/**
				 * Hook: woocommerce_shop_loop.
				 */
				do_action('woocommerce_shop_loop');
			?>
				<?php wc_get_template_part('content', 'product'); ?>
		<?php
			endwhile;
		endif;
		woocommerce_product_loop_end();

	$product_list_block = ob_get_clean();

	ob_start();
		woocommerce_product_loop_start();
		if (wc_get_loop_prop('total')) :
			while (have_posts()) :
				the_post();
				/**
				 * Hook: woocommerce_shop_loop.
				 */
				do_action('woocommerce_shop_loop');
			?>
				<?php wc_get_template_part('content', 'product-gorisontal'); ?>
		<?php
			endwhile;
		endif;
		woocommerce_product_loop_end();

	$product_list_goris = ob_get_clean();

	
	echo (json_encode(array(
		'product_list_block' => $product_list_block,
		'product_list_goris' => $product_list_goris
	)));
	die();
}

function get_table_product() {
	$args = unserialize(stripslashes($_POST['query']));
	$args['post_status'] = 'publish';
	$args['posts_per_page'] = 1000;

	query_posts($args);

	woocommerce_product_loop_start();
	if (wc_get_loop_prop('total')) {
		
		while (have_posts()) {
			the_post();
			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action('woocommerce_shop_loop');

			wc_get_template_part('content', 'product-line');
		}
	}
	woocommerce_product_loop_end();

	die();
}

if( wp_doing_ajax() ){
	add_action('wp_ajax_loadmore', 'loadmore_get_posts');
	add_action('wp_ajax_nopriv_loadmore', 'loadmore_get_posts');
	add_action('wp_ajax_loadmore_products', 'loadmore_get_products');
	add_action('wp_ajax_nopriv_loadmore_products', 'loadmore_get_products');
	add_action('wp_ajax_get_table_product', 'get_table_product');
	add_action('wp_ajax_nopriv_get_table_product', 'get_table_product');
}

function gpsswitcher_polylang_languages($class = '')
{
	if (!function_exists('pll_the_languages')) return;
	$languages = pll_the_languages(array(
		'display_names_as'       => 'slug',
		'hide_if_no_translation' => 1,
		'show_flags' => 1,
		'raw'                    => true
	));
	if (!empty($languages)) {
		$output = '<div class="' . $class . '">';
		foreach ($languages as $language) {
			$id             = $language['id'];
			$slug           = $language['slug'];
			$url            = $language['url'];
			$flag           = $language['flag'];
			$current        = $language['current_lang'] ? ' active' : ' disabled';
			$no_translation = $language['no_translation'];
			if (!$no_translation) {
				if ($current == ' active') {
					$output .= "<a class=\"langButton\" href=\"#\"></a>";
				}
			}
		}
		$output .= '<ul role="menu" class="lang">';
		foreach ($languages as $language) {
			$id             = $language['id'];
			$slug           = $language['slug'];
			$url            = $language['url'];
			$flag           = $language['flag'];
			$current        = $language['current_lang'] ? ' active' : ' disabled';
			$no_translation = $language['no_translation'];
			if (!$no_translation) {

				$output .= "<li class=\"" . $current . "\">
							  <a title=\"\" href=\"$url\" hreflang=\"$slug\" lang=\"$slug\">
							  $slug</a>";
			}
		}
		$output .= '</ul></div>';
	}
	return $output;
}

add_action('premmerce_product_filter_render', 'new_england_premmerce_product_filter_render', -1);

function new_england_premmerce_product_filter_render($data)
{
	extract($data);
	include "inc/filter.php";
}

add_action('premmerce_product_filter_render_widget', 'new_england_premmerce_product_filter_render_widget', -1);

function new_england_premmerce_product_filter_render_widget($data)
{
	extract($data);
	include "inc/widget_filters.php";
}

add_action('premmerce_product_active_filters_render', 'new_england_premmerce_product_active_filters_render', -1);

function new_england_premmerce_product_active_filters_render($data)
{
	extract($data);
	include "inc/active_filters.php";
}

add_action('premmerce_filter_render_item_checkbox', 'shopico_tmp_checkbox_override_premmerce_filter', 1);


add_action('premmerce_filter_popup_render_item_checkbox', 'new_england_premmerce_filter_popup_render_item_checkbox', 10);

/** * @param $attribute */
function new_england_premmerce_filter_popup_render_item_checkbox($attribute){
	$__variables = array('attribute' => $attribute);
	extract($__variables);
	
	include get_template_directory().'/inc/premmerce-woocommerce-product-filter/views/types/popup/checkbox.php';
}

add_action('premmerce_filter_popup_render_item_slider', 'new_england_premmerce_filter_popup_render_item_slider', 10);

/** * @param $attribute */
function new_england_premmerce_filter_popup_render_item_slider($attribute){
	$__variables = array('attribute' => $attribute);
	extract($__variables);
	include get_template_directory().'/inc/premmerce-woocommerce-product-filter/views/types/popup/slider.php';
}

add_action('premmerce_filter_popup_render_item_radio', 'new_england_premmerce_filter_popup_render_item_radio', 10);

/** * @param $attribute */
function new_england_premmerce_filter_popup_render_item_radio($attribute){
	$__variables = array('attribute' => $attribute);
	extract($__variables);
	include get_template_directory().'/inc/premmerce-woocommerce-product-filter/views/types/popup/radio.php';
}

add_action('premmerce_filter_popup_render_item_select', 'new_england_premmerce_filter_popup_render_item_select', 10);

/** * @param $attribute */
function new_england_premmerce_filter_popup_render_item_select($attribute){
	$__variables = array('attribute' => $attribute);
	extract($__variables);
	include get_template_directory().'/inc/premmerce-woocommerce-product-filter/views/types/popup/select.php';
}
// Add share buttons to post
require_once(get_template_directory() . '/template-parts/share.php');

// Add share buttons to product popup pdf
require_once(get_template_directory() . '/template-parts/share_popup.php');

//Add function for save in pdf
require_once(get_template_directory() . '/template-parts/pdfinit.php');
//add_action( 'wp_ajax_me_post_pdf', 'me_post_pdf_ajax' ); // wp_ajax_{ЗНАЧЕНИЕ ПАРАМЕТРА ACTION!!}
//add_action( 'wp_ajax_nopriv_me_post_pdf', 'me_post_pdf_ajax' );  // wp_ajax_nopriv_{ЗНАЧЕНИЕ ACTION!!}
// первый хук для авторизованных, второй для не авторизованных пользователей
// Set some content to print

function pdfhtmls($post_id) {
	$data = array('post_id'=>$post_id);
	get_template_part( 'template-parts/pdfhtml', 'post_id', $data );
  //  require_once(get_template_directory() . '/template-parts/pdfhtml.php');
}

function me_post_pdf_ajax()
{
	if (isset($_POST['me_post_pdf_ajax'])){
//	$namepdf = $_POST['pdfname'];
        $pdfcontents = $_POST['pdfcontents'];
        $post_id = $_POST['post_id'];
		require_once(get_template_directory() . '/tcpdf/tcpdf.php');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetFont( 'Arial', 'I', 14);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('New England');
		$pdf->SetTitle('New England');
		$pdf->SetSubject('PDF New England');
		$pdf->SetKeywords('TCPDF, PDF New England');
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language-dependent strings (optional)
if (@file_exists(get_template_directory() . '/tcpdf/lang/ukr.php')) {
	require_once(get_template_directory() . '/tcpdf/lang/ukr.php');
	$pdf->setLanguageArray($l);
}
//		$pdf->setHeaderFont(Array('freeserif', '', 10, '', false));
//		$pdf->setFooterFont(Array('freeserif', '', 8, '', false));
//		$pdf->SetFont('freeserif', '', 10, '', false);
// add a page
		$pdf->AddPage('P');
//$pdf->SetDisplayMode('real','default');
//$path_to_ttf = get_template_directory() . '/tcpdf/fonts/arial.php';
//$fontname = TCPDF_FONTS::addTTFfont ($path_to_ttf , 'TrueTypeUnicode', '', 96);
//$pdf->SetFont ($fontname, '', 14, '', false);
// Print text using writeHTMLCell()
//$pdf->writeHTMLCell(0, 0, '', '', pdfhtmls('58'), 0, 1, 0, true, '', true);

		$html = '<html>
<head></head>
<body>
     '.$pdfcontents.'
</body>
</html>';

		$pdf->writeHTML($html, true, 0, true, 0);
		$pdf->Output('example.pdf', 'D');
		//$pdf->Output('example.pdf', 'S');
///////// ІНШИЙ ПДФ /////////////////////////////////
	}
}
add_action('init', 'me_post_pdf_ajax');


/* 
* add phone masked input 
*/
add_action('wp_footer', 'new_england_activate_masked_input');
function new_england_activate_masked_input()
{
	?>
	<script>
		jQuery(document).ready(function($) {
			$(".user_telephone").mask("+38(999)-999-99-99");
			console.log("++");
		});
	</script>
<?php
}


/* Регестрируем строки для перевода в Полиленг*/

pll_register_string( 'single-page-pll', 'зв’яжіться з нами' );
pll_register_string( 'single-page-pll-view', 'записатись на огляд квартири' );
pll_register_string( 'single-page-pll-pdf', 'скачати PDF' );

pll_register_string( 'single-page-pll-curs', 'курс валют' );
pll_register_string( 'single-page-pll-parking', 'Підземний паркинг' );
pll_register_string( 'most-beautiful', 'Тільки найкраще' );
pll_register_string( 'akcii', 'Акції' );
pll_register_string( 'all', 'Всі' );
pll_register_string( 'sale-build', 'Знижки на нерухомість' );
pll_register_string( 'catalog-apartament', 'Каталог квартир' );
pll_register_string( '1-apartament', '1-кімнатні' );
pll_register_string( '2-apartament', '2-кімнатні' );
pll_register_string( '3-apartament', '3-кімнатні' );
pll_register_string( 'in-catalog', 'перейти в каталог' );
pll_register_string( 'розгорнути фільтр', 'розгорнути фільтр' );
pll_register_string( 'Поля таблицы', 'Поля таблицы' );
pll_register_string( 'Від', 'От', '' );
pll_register_string( 'До', 'До', 'To' );

pll_register_string( 'filter', 'Дім' );
pll_register_string( 'filter2', 'Площа м²' );
pll_register_string( 'filter3', 'К-сть кімнат' );
pll_register_string( 'filter4', 'Загальна сума' );
pll_register_string( 'filter5', 'Фільтр' );

/* Добавляем номер сообщениям CF7*/

function wpcf7_generate_rand_number( $wpcf7_data ) {
		$properties = $wpcf7_data->get_properties();
		$shortcode = '[rand-generator]';
		$mail = $properties['mail']['body'];
		$mail_2 = $properties['mail_2']['body'];
	
		$subject = $properties['mail']['subject'];
		$subject2 = $properties['mail_2']['subject'];
	
		if( preg_match( "/{$shortcode}/", $mail ) || preg_match( "/[{$shortcode}]/", $mail_2 ) ) {
			$option = 'wpcf7sg_' . $wpcf7_data->id();
			$sequence_number = (int)get_option( $option ) + 1;
			update_option( $option, $sequence_number );
			$properties['mail']['body'] = str_replace( $shortcode, $sequence_number, $mail );
			$properties['mail_2']['body'] = str_replace( $shortcode, $sequence_number, $mail_2 );
			$properties['mail']['subject'] = str_replace( $shortcode, $sequence_number, $subject );
			$properties['mail_2']['subject'] = str_replace( $shortcode, $sequence_number, $subject2 );
	
			$wpcf7_data->set_properties( $properties );
		}
	}
	add_action( 'wpcf7_before_send_mail', 'wpcf7_generate_rand_number' );