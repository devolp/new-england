<?php echo '<?xml version="1.0" encoding="utf-8" ?>'; ?>
 <!DOCTYPE html 
 PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta charset="utf-8">
<title>Title PDF</title>
<base href="http://anglia.com" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoiceproa.css" />
</head>
<body style="">
<div class="print_header"
style="display: flex;justify-content: space-between;padding-bottom: 23px;margin-bottom: 22px;border-bottom: 1px solid #e6e5de;">
<a href="#" class="print_logo"
style="background-image: url(https://work2.devolp.com/wp-content/uploads/2021/08/NewEngland-logo-UA.svg);"></a>
<div class="print_contact_info" style="display: flex;flex-direction: column;">
<p class="print_contact_text" style="font-size: 11px;line-height: 120%;color: #1c1f1e;margin: 0;">+38
(044) 290 73 48</p>
<p class="print_contact_text" style="font-size: 11px;line-height: 120%;color: #1c1f1e;margin: 0;">м.
Київ, вул. М. Максимовича, 24</p>
</div>
</div>
<div class="print_item_container">
<div class="print_item">
<h2 class="print_item_title">Квартира 112</h2>
<p class="print_item_suptitle">111Дом Лондон (сдан в эксплуатацию 4 кв. 2021 года).
ул. Максимовича – (№ дома В-4).
Помещение под офис, магазин или кофейню. Помещение с 4-мя входами. Вид из окна: дом Ньюкасл.
Состояние после строителей – под чистовую отделку.</p>
<div class="print_item_charasteristick">
<div class="print_size">
<p>85,72м²</p>
<p>Площа</p>
</div>
<div class="name_of_house">
<p>Лондон</p>
<p>Будинок</p>
</div>
<div class="flor">
<p>1</p>
<p>Поверх</p>
</div>
</div>
<div class="print_prise">
<p>4 260 980<span>грн.</span></p>
<div class="prise_in_usa">
<p>157 000 $</p>
<a href="#" class="exchange_rate">курс валют</a>
</div>
</div>
</div>
<div class="print_item_last">
<img src="https://work2.devolp.com/wp-content/uploads/2021/07/image-9-1.jpg" alt="Photo_flet">
</div>
</div>
<div class="print_photo_container">
<img src="https://work2.devolp.com/wp-content/uploads/2021/07/image-13-3.jpg" alt="Photo_flat1">
<img src="https://work2.devolp.com/wp-content/uploads/2021/07/image-13-2.jpg" alt="Photo_flat2">
<img src="https://work2.devolp.com/wp-content/uploads/2021/07/image-13-1.jpg" alt="Photo_flat3">
<img src="https://work2.devolp.com/wp-content/uploads/2021/07/image-13.jpg" alt="Photo_flat4">
<img src="https://work2.devolp.com/wp-content/uploads/2021/07/image-12.jpg" alt="Photo_flat5">
</div>
<div class="real_estate_tab parking">
<span class="tab_button"></span>
<p>Підземний паркинг</p>
</div>
<div class="card_map_item list">
<ul class="card_map_list">
<li>
<h3 class="list_title">Характеристики</h3>
</li>
<li>Без додаткових платежів</li>
<li>Стіни з цегли</li>
<li>Індивідуальне опалення</li>
<li>Пропозиція від посередника</li>
</ul>
<ul class="card_map_list">
<li>
<h3 class="list_title">Інфраструктура</h3>
</li>
<li>Магазин</li>
<li>Дитячий садок</li>
<li>Нова Пошта</li>
<li>Парковка</li>
</ul>
</div>
</div>
</body>
</html>