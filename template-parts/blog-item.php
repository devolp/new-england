<div class="blog-item">
    <a href="<?php the_permalink(); ?>">
        <div class="blog-image">
            <?php if (has_post_thumbnail()) :
                the_post_thumbnail();
            endif; ?>
        </div>
    </a>
    <span class="blog-date"><?php $date_format = get_option('date_format');
                            echo  get_the_date($date_format); ?></span>
    <a href="<?php the_permalink(); ?>">
        <p class="blog-text"><?php the_title(); ?></p>
    </a>
</div>