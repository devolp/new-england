<?php
add_action('pdfContent', 'pdfContent', 10);
header('Content-Type: text/html; charset=UTF-8');

mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_regex_encoding('UTF-8');

function pdfContent(){
	$postTitel = get_the_title();
	$suffix = __('Нова Англія');
	$postUrl = get_the_permalink(get_the_ID());
	$mess_start = __('Я думаю, що ця сторінка, яку я знайшов на Нова Англія, може вас зацікавити:');
  $copy_message = get_field('copy_post_onclick_message', 'option');
	global $product;
    $pllang = pll_current_language();
    $lang = get_field($pllang, 'option');
?>

<div class="print_header" style="display: flex;
  justify-content: space-between;
  padding-bottom: 23px;
  margin-bottom: 22px;
  border-bottom: 1px solid #e6e5de;">
                <a href="#" class="print_logo" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/product_card/print_logo.svg);"></a>
                <div class="print_contact_info" style="
  display: flex;
  flex-direction: column;">
                    <div class="print_contact_text" style="font-size: 11px;
  line-height: 120%;
  color: #000;
  margin: 0;"><?php echo $lang['footer_phone']; ?></div>
                    <div class="print_contact_text" style="font-size: 11px;
  line-height: 120%;
  color: #000;
  margin: 0;"><?php echo $lang['adress']; ?></div>
                </div>
            </div>
            <div class="print_item_container">
                    <div class="print_item">
                        <h2 class="print_item_title"><?=$postTitel?></h2>
                        <div class="print_item_suptitle"><?php the_content(); ?></div>
                        <div class="print_item_charasteristick">
                            <div class="print_size">
                                <p> <?php echo $product->get_attribute( 'pa_polscha' ); ?> <?php echo __( 'м²', 'new-england' ); ?></p>
                                <p><?php echo wc_attribute_label( 'pa_polscha' ); ?></p>
                            </div>
                            <div class="name_of_house">
                                <p><?php echo $product->get_attribute( 'pa_building' ); ?></p>
                                <p><?php echo wc_attribute_label( 'pa_building' ); ?></p>
                            </div>
                            <div class="flor">
                                <p><?php echo $product->get_attribute( 'pa_floor' ); ?></p>
                                <p><?php echo wc_attribute_label( 'pa_floor' ); ?></p>
                            </div>
                        </div>
                        <div  class="print_prise">
	                        <?php $pa_polscha = str_replace(",", ".",$product->get_attribute( 'pa_polscha' ));
	                        $f_price = (float)$product->get_price();  ?>
                            <p class="prise_in grivna-print"><?php global $WOOCS; // echo $product->get_price_html(); ?>
		                        <?php $price_total = ($pa_polscha * $product->get_price()); ?>

                                <span id="prise_in" data-parking="<?php echo $WOOCS->woocs_exchange_value(get_field('price_parking')); ?>" data-total="<?php echo $price_total; ?>"><?php echo wc_price( $price_total ); ?></span>
                            </p>
                            <div class="prise_in_usa">
                                <p><?php echo $product->get_price_html(); ?></p>
                                <span class="exchange_rate"><?php echo __( 'м²', 'new-england' ); ?></span>
                            </div>
                        </div>
                        <div class="card_prise_parking">
                            <div class="real_estate_tab parking">
                                <input disabled readonly type="checkbox" name="product_checkbox" id="product_checkbox_2" class="tab_button">
                                <label for="product_checkbox_2"><?php echo pll_e('Підземний паркинг'); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="print_item_last">
                        <?php $image_data_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                        <img src="<?php echo $image_data_full[0]; ?>" alt="Photo_flet">
                    </div>
            </div>
            <?php $attachment_ids = $product->get_gallery_image_ids(); ?>
            <div class="print_photo_container">
                <?php foreach ( $attachment_ids as $attachment_id ) {
                $full_src = wp_get_attachment_image_src( $attachment_id, 'full' );?>
                    <img src="<?php echo $full_src[0]; ?>" alt="Photo_flat">
                <?php } ?>
            </div>
    <?php /*
            <div class="real_estate_tab parking">
                    <span class="tab_button"></span>
                    <p><?php echo pll_e('Підземний паркинг'); ?></p>
            </div> */ ?>
    <div class="card_map_item list">
	<?php if( have_rows('add_characteris') ): ?>
		<?php while( have_rows('add_characteris') ): the_row(); ?>
			<?php $title_section = get_sub_field('title_section'); ?>
            <ul class="card_map_list">
                <li><h3 class="list_title"><?php echo esc_attr( $title_section ); ?></h3></li>
				<?php if( have_rows('add_character') ): ?>
					<?php while( have_rows('add_character') ): the_row(); ?>
						<?php $value_character = get_sub_field('value_character'); ?>
                        <li><?php echo esc_attr( $value_character ); ?></li>

					<?php endwhile; ?>
				<?php endif; ?>
            </ul>
		<?php endwhile; ?>
	<?php endif; ?>

    <?php if( have_rows('add_infrastructure') ): ?>
        <?php while( have_rows('add_infrastructure') ): the_row(); ?>
            <?php $title_section = get_sub_field('title_section_infrastructura'); ?>
            <ul class="card_map_list">
                <li><h3 class="list_title"><?php echo esc_attr( $title_section ); ?></h3></li>
                <?php if( have_rows('add_infrastruct') ): ?>
                    <?php while( have_rows('add_infrastruct') ): the_row(); ?>
                        <?php $value_character = get_sub_field('value_infrastructure'); ?>
                        <li><?php echo esc_attr( $value_character ); ?></li>

                    <?php endwhile; ?>
                <?php endif; ?>
            </ul>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>
  </div>


<?php
}