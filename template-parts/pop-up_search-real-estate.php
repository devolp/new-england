<?php
$pllang = pll_current_language();
$lang = get_field($pllang, 'option');
?>

<section class="popap search">
    <span class="popap_close"></span>
    
    <?php if (is_active_sidebar('popup_filters')) : ?>
        <?php dynamic_sidebar('popup_filters'); ?>
    <?php endif; ?>
</section>