 <?php
    $pllang = pll_current_language();
    $lang = get_field($pllang, 'option');
    ?>

 <section class="popap contact">
     <span class="popap_close"></span>
     <div class="popap_container">
         <?php echo $lang['text_modal_window_contact']; ?>
         <?php echo do_shortcode('[cf7form cf7key="%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%bd%d0%b0-%d1%84%d0%be%d1%80%d0%bc%d0%b0-%d0%b7%d0%b0%d0%bf%d0%b8%d1%81%d0%b0%d1%82%d0%b8%d1%81%d1%8f-%d0%bd%d0%b0-%d0%be%d0%b3%d0%bb%d1%8f%d0%b4-%d0%ba-3"]'); ?>
     </div>
 </section>