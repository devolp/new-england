 <?php
    $pllang = pll_current_language();
    $lang = get_field($pllang, 'option');
    ?>

 <section class="popap inspect_us modal_window" id="inspect-apartment">
     <span class="popap_close"></span>
     <div class="popap_container">
         <?php echo $lang['text_modal_window_inspect-apartment']; ?>
         <?php echo do_shortcode('[cf7form cf7key="contact-form-make_an_appointment-to-inspect_the_apartment"]'); ?>
     </div>
 </section>