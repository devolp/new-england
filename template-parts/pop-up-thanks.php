 <?php
    $pllang = pll_current_language();
    $lang = get_field($pllang, 'option');
    ?>
 <section id="thanks" class="modal_window popap thank_you">
     <span class="popap_close"></span>
     <div class="thanks-inner popap_container">
         <?php echo $lang['text_modal_window_have_questions']; ?>
     </div>
 </section>