 <?php

    get_header();

    get_template_part('woocommerce/global/breadcrumb');
    ?>
 <section class="article">
     <div class="container">
         <div class="article-content">
             <?php if (have_posts()) :
                    while (have_posts()) :
                        the_post(); ?>
                     <?php if (get_field('article-author-image') && get_field('article-about-author')) : ?>
                         <div class="article-col-left">
                             <div class="article-image">
                                 <?php $article_author_img = get_field('article-author-image'); ?>
                                 <img src="<?php echo esc_url($article_author_img['url']); ?>" alt="<?php echo esc_attr($article_author_img['alt']); ?>">
                             </div>
                             <div class="article-text">
                                 <p class="article-title">
                                     <?php echo  the_field('article-about-author'); ?>
                                 </p>
                                 <p class="article-subtitle">
                                     <?php echo  the_field('article-job-author'); ?>
                                 </p>
                             </div>
                         </div>
                     <?php endif; ?>
                     <div class="article-inner">
                         <div class="article-col-right">
                             <p class="article-date"><?php echo get_the_date('d/m/Y'); ?></p>
                             <h2 class="article-main-text"><?php the_title(); ?></h2>
                             <div class="article-main-image">
                                 <?php the_post_thumbnail('full') ?>
                             </div>
                             <div class="article-text-wrap">
                                 <?php the_content(); ?>
                             </div>
                         </div>
                     </div>
         </div>
 </section>
 <?php
                    endwhile;
                endif;
    ?>

 <?php
    get_template_part('template-parts/have-any-questions-section');

    get_footer();
    ?>