  <?php
    /**
     * Template Name: template for the page contacts
     *
     * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
     *
     * @package New_England
     */
    get_header();

    get_template_part('woocommerce/global/breadcrumb');
    ?>
  <section class="contacts">
      <div class="container">
          <div class="contacts_top">
              <?php $contacts_top = get_field('contacts_top');
                if ($contacts_top) : ?>
                  <div class="contacts_top_left">
                      <h2><?php echo $contacts_top['contacts_title']; ?></h2>
                  </div>
                  <div class="contacts_top_right">
                      <p><?php echo $contacts_top['contacts-text']; ?></p>
                      <div class="contacts_btn">
                          <?php $link = $contacts_top['contacts-btn'];
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                              <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" class="btn">
                                  <?php echo esc_html($link_title); ?>
                              </a>
                          <?php endif; ?>
                      </div>
                  </div>
              <?php endif; ?>
          </div>

          <div class="contacts_mid">
              <div class="contacts_cart">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2543.5854271298417!2d30.47283891594243!3d50.39292639938887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4c8ded6de7ba1%3A0x38cc3fdae1394c34!2z0LLRg9C70LjRhtGPINCc0LjRhdCw0LnQu9CwINCc0LDQutGB0LjQvNC-0LLQuNGH0LAsIDI0LCDQmtC40ZfQsiwgMDIwMDA!5e0!3m2!1suk!2sua!4v1627470500386!5m2!1suk!2sua" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              </div>
          </div>

          <div class="contacts_bottom">
              <?php $contacts_bottom = get_field('contacts_bottom');
                if ($contacts_bottom) : ?>
                  <div class="contacts_top_left_bt">
                      <h2><?php echo $contacts_bottom['contacts_title_2']; ?></h2>
                  </div>
                  <div class="contacts_top_right">
                      <div class="contacts_bottom_link">
                          <p><?php echo $contacts_bottom['contacts-email-label']; ?></p>
                          <a href="mailto:<?php echo $contacts_bottom['contacts-email']; ?>">
                              <?php echo $contacts_bottom['contacts-email']; ?></a>
                      </div>
                      <div class="contacts_bottom_link">
                          <p><?php echo $contacts_bottom['contacts-phone-label']; ?></p>
                          <a href="tel:<?= preg_replace('/[^0-9]/', '', $contacts_bottom['contacts-phone']); ?>"><?php echo $contacts_bottom['contacts-phone']; ?></a>
                      </div>
                  </div>
              <?php endif; ?>
          </div>
      </div>
  </section>

  <section class="question_form _anim-no-hide _anim_items">
      <div class="qestion_container _amin-show">
          <?php echo do_shortcode('[cf7form cf7key="%d1%84%d0%be%d1%80%d0%bc%d0%b0-%d0%b7%d0%b0%d0%bb%d0%b8%d1%88%d0%b8%d0%bb%d0%b8%d1%81%d1%8f-%d0%bf%d0%b8%d1%82%d0%b0%d0%bd%d0%bd%d1%8f_copy"]'); ?>
      </div>
  </section>

  <?php
    get_footer(); ?>