<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package New_England
 */

get_header();
?>
	<main id="primary" class="site-main">

		<section class="error-404 not-found">
			<div class="block1">
				<h1 class="not_found_title">404 - помилка</h1>
				<p class="not_found_suptitle">На сайті виявлена ​​помилка! Будь ласка, поверніться на головну сторінку і продовжуйте пошуки.</p>
				<a class="not_found_button" href="<?php echo pll_home_url(); ?>">На головну</a>
			</div>
			<div class="not_found_img">
				<img src="http://work2.devolp.com/wp-content/uploads/2021/09/404.png" alt="Photo">
			</div>
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
?>
