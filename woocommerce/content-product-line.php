<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product-gorisontal.php.
 *
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<a href="<?php echo esc_url(get_the_permalink()) ?>" class="filter_table_result_container active-product-line" data-id="<?php echo $product->get_ID(); ?>">
    <div class="filter_table_item first">
        <span class="nummer_section_result"><?php echo $product->get_attribute( 'pa_num-section' ); ?></span>
        <span class="flor_result flor_result_fulls"><?php echo $product->get_attribute( 'pa_floor' ); ?></span>
        <span class="rum_result rum_result_fulls"><?php echo $product->get_attribute( 'pa_kimnat' ); ?></span>
    </div>
    <div class="filter_table_item two">
        <span class="area_m2_section_result area_m2_section_result_firsts"><?php echo $product->get_attribute( 'pa_polscha' ); ?></span>
        <span class="house_result"><?php echo $product->get_attribute( 'pa_building' ); ?></span>
        <span class="nummer_flet_result nummer_flet_result_fulls"><?php echo $product->get_attribute( 'pa_num-apartment' ); ?></span>
    </div>
    <div class="filter_table_item last">
        <span class="real_estate_section_result"><?php echo $product->get_attribute( 'pa_type-neruhomosti' ); ?></span>
        <span class="total_result total_result_firsts"><?php echo(intval($product->get_price()) * intval($product->get_attribute('pa_polscha'))) ?></span>
        <span class="status_result"><?php echo $product->get_attribute( 'pa_status' ); ?></span>
    </div>
</a>
