<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
	<?php 
	$pllang = pll_current_language();
	$lang = get_field( $pllang, 'option' );
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

    <section class="product_card_main_screen">

    <?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */

        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
		do_action( 'woocommerce_single_product_summary' );
		?>

        <div class="card_item first">
      <?php the_title('<h1 class="card_title">', '</h1>' ); ?>
            <p class="card_main_text newreadmore">
                <?php the_content(); ?>
            </p>
            <div class="card_flex_charasteristick">
                <div class="charasteristick_item">
                    <p class="charasteristick_bold"><?php echo $product->get_attribute( 'pa_polscha' ); ?> <?php echo __( 'м²', 'new-england' ); ?></p>
                    <p class="charasteristick_normal"><?php echo wc_attribute_label( 'pa_polscha' ); ?></p>
                                   </div>
                <div class="charasteristick_item">
                    <p class="charasteristick_bold"><?php echo $product->get_attribute( 'pa_building' ); ?></p>
                    <p class="charasteristick_normal"><?php echo wc_attribute_label( 'pa_building' ); ?></p>
                </div>
                <div class="charasteristick_item">
                    <p class="charasteristick_bold"><?php echo $product->get_attribute( 'pa_floor' ); ?></p>
                    <p class="charasteristick_normal"><?php echo wc_attribute_label( 'pa_floor' ); ?></p>
                </div>
                <div class="card_prise_item mobile">
                  <p class="prise_in dolar">
                    <?php echo $product->get_price_html(); ?></p>
                        <span class="charasteristick_normal"><?php echo __( 'м²', 'new-england' ); ?></span>
                </div>
            </div>
            <div class="card_mobile_container">
                <div class="card_flet_prise">
                    <div class="card_prise_item">
	                    <?php $pa_polscha = str_replace(",", ".",$product->get_attribute( 'pa_polscha' ));
	                    $f_price = (float)$product->get_price();  ?>
                        <p class="prise_in grivna"><?php global $WOOCS; // echo $product->get_price_html(); ?>
                            <?php $price_total = ($pa_polscha * $product->get_price()); ?>

                            <span id="prise_in" data-parking="<?php echo $WOOCS->woocs_exchange_value(get_field('price_parking')); ?>" data-total="<?php echo $price_total; ?>"><?php echo wc_price( $price_total ); ?></span>
                        </p>
                        <?php if(get_field('price_parking') > 0) { ?>
                        <div class="card_prise_parking">
                            <div class="real_estate_tab parking">
                                <input onclick="addparking()" type="checkbox" name="product_checkbox" id="product_checkbox" class="tab_button">
                                <label for="product_checkbox"><?php echo pll_e('Підземний паркинг'); ?></label>
                            </div>
                        </div>
                        <?php } ?>

                    </div>

                    <div class="card_prise_item">
                        <p class="prise_in dolar">
	                        <?php echo $product->get_price_html(); ?></p>
                        <span class="exchange_rate"><?php echo __( 'м²', 'new-england' ); ?></span>
                    </div>
                </div>
                <div class="card_contact_us">
                    <a href="#" class="contact_as" data-title-product="<?php the_title(); ?>" data-build-name="<?php echo $product->get_attribute( 'pa_building' ); ?>"><?php echo pll_e('зв’яжіться з нами'); ?></a>
                    <a download href="<?php echo get_field('full_plan_pdf'); ?>" class="inspect"><?php echo pll_e('скачати PDF'); ?></a>
                </div>
            </div>
        </div>
	    <?php
	    $attachment_ids = $product->get_gallery_image_ids();
	    $image_data_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
	    $video_slider = get_field('add_video');
	    ?>
        <div class="card_item_last_container">
            <div>
                <div class="slider slider-for-products">
                    <div>
                        <a data-fancybox="gallery" href="<?php echo esc_url( $image_data_full[0] ); ?>">
                            <img class="" src="<?php echo esc_url( $image_data_full[0] ); ?>" alt="Photo">
                        </a>
                    </div>
	                <?php if($video_slider) { ?>
                        <div>
                            <a data-fancybox="gallery" class="play_video" href="<?php the_field('add_video');?>" data-fancybox >
                                <img src="<?php the_field('image_video');?>" class="">
                            </a>
                        </div>
	                <?php } ?>
	                <?php
	                foreach ( $attachment_ids as $attachment_id ) {
		                $full_src = wp_get_attachment_image_src( $attachment_id, 'full' );
		                ?>
                        <div>
                            <a data-fancybox="gallery" href="<?php echo esc_url( $full_src[0] ); ?>">
                                <img class="" src="<?php echo $full_src[0]; ?>" alt="Photo">
                            </a>
                        </div>
	                <?php } ?>
                </div>
                <div class="slider slider-nav-products">
                    <div>
<!--                        <a data-fancybox="gallery" href="--><?php //echo esc_url( $image_data_full[0] ); ?><!--">-->
                            <div class="img_container"><img class="" src="<?php echo esc_url( $image_data_full[0] ); ?>" alt="Photo"></div>
<!--                        </a>-->
                    </div>
	                <?php if($video_slider) { ?>
                        <div>
<!--                            <a data-fancybox="gallery" class="play_video" href="--><?php //the_field('add_video');?><!--" data-fancybox >-->
                                <div class="img_container"><img src="<?php the_field('image_video');?>" class=""></div>
<!--                            </a>-->
                        </div>
	                <?php } ?>
	                <?php
	                foreach ( $attachment_ids as $attachment_id ) {
		                $full_src = wp_get_attachment_image_src( $attachment_id, 'full' );
		                ?>
                        <div>
<!--                            <a data-fancybox="gallery" href="--><?php //echo esc_url( $image_data_full[0] ); ?><!--">-->
                                <div class="img_container"><img class="" src="<?php echo $full_src[0]; ?>" alt="Photo"></div>
<!--                            </a>-->
                        </div>
	                <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <section class="card_map">
        <div class="card_map_item list ">
        
        <?php if( have_rows('add_characteris') ): ?>
        <?php while( have_rows('add_characteris') ): the_row(); ?>
    <?php $title_section = get_sub_field('title_section'); ?>
        <h3 class="list_title"><?php echo esc_attr( $title_section ); ?></h3>

        

            <ul class="card_map_list">
            <?php if( have_rows('add_character') ): ?>
    <?php while( have_rows('add_character') ): the_row(); ?>
    <?php $value_character = get_sub_field('value_character'); ?>
                <li><?php echo esc_attr( $value_character ); ?></li>

                <?php endwhile; ?>
<?php endif; ?>
            </ul>

            <?php endwhile; ?>
<?php endif; ?>


<?php if( have_rows('add_infrastructure') ): ?>
        <?php while( have_rows('add_infrastructure') ): the_row(); ?>
        <?php $title_section_infrastructura = get_sub_field('title_section_infrastructura'); ?>
            <h3 class="list_title"><?php echo esc_attr( $title_section_infrastructura ); ?></h3>
            <ul class="card_map_list">
            <?php if( have_rows('add_infrastruct') ): ?>
    <?php while( have_rows('add_infrastruct') ): the_row(); ?>
    <?php $value_infrastructure = get_sub_field('value_infrastructure'); ?>
    <li><?php echo esc_attr( $value_infrastructure ); ?></li>
                <?php endwhile; ?>
<?php endif; ?>

            </ul>

            
            <?php endwhile; ?>
<?php endif; ?>

        </div>

        <?php if( have_rows('add_maps') ): ?>
        <?php while( have_rows('add_maps') ): the_row(); ?>
        <?php $latitude = get_sub_field('latitude'); ?>
        <?php $longitude = get_sub_field('longitude'); ?>
        <?php if($latitude && $longitude): ?>
        <div class="card_map_container " id="map">
                <script>
                  var map;
                  var myLatLng = {lat: <?php echo $latitude; ?>, lng:  <?php echo $longitude; ?>};
                  function initMap() {
                    map = new google.maps.Map(document.getElementById("map"), {
                      center: myLatLng,
                      zoom: 17,
                      disableDefaultUI: true
                    });
                    
                    var marker = new google.maps.Marker({
                    position: {lat: <?php echo $latitude; ?>, lng:  <?php echo $longitude; ?>},
                    map: map,
                    });

                    var styles = [
  {
    "featureType": "poi.attraction",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi.business",
    "stylers": [
      {
        "saturation": 5
      },
      {
        "lightness": 5
      },
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.government",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi.medical",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.place_of_worship",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi.school",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  }
]
              map.setOptions({styles: styles});
              //map.setZoom(1);
                  }
                </script>
                <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWWL5MNY3ct8daaBxk0MXeY4sTh8IU5aw&callback=initMap&language=ru"></script>
        </div>
        <?php endif; ?>
        <?php endwhile; ?>
<?php endif; ?>
    </section>





    <section class="parking ">
        <h2 class="parking_title "><?php echo pll_e('Підземний паркинг'); ?></h2>
        <div class="parking_container ">
        <?php
   $args_parking = array(
    'post_type'      => 'product',
    'posts_per_page' => 3,
    'product_cat'    => 'underground-parking',
    'orderby' => 'rand',

);

$parking = new WP_Query( $args_parking );

while ( $parking->have_posts() ) : $parking->the_post();
global $product; ?>
            <div class="parking_item">
            <a href="<?php echo get_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Photo_parking"></a>
                
                <div class="parking_item_footer">
                    <p class="parking_text"><?php echo get_the_title(); ?></p>
                    <p class="parking_prise"><?php echo $product->get_price_html(); ?></p>
                </div>
                <a href="<?php echo get_permalink(); ?>" class="result_item_button"><span class="arrow"></span></a>
            </div>   
            <?php  endwhile; ?>
<?php   wp_reset_postdata(); ?>

        </div>
        <div class="parking_container_slider ">

        <?php
   $args_parking2 = array(
    'post_type'      => 'product',
    'posts_per_page' => 3,
    'product_cat'    => 'underground-parking',
    'orderby' => 'rand',

);

$parking2 = new WP_Query( $args_parking2 );

while ( $parking2->have_posts() ) : $parking2->the_post();
global $product; ?>
            <div class="parking_item">
            <a href="<?php echo get_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Photo_parking"></a>

                <div class="parking_item_footer">
                    <p class="parking_text"><?php echo get_the_title(); ?></p>
                    <p class="parking_prise"><?php echo $product->get_price_html(); ?></p>
                </div>
                <a href="<?php echo get_permalink(); ?>" class="result_item_button"><span class="arrow"></span></a>
            </div>  
            <?php  endwhile; ?>
<?php   wp_reset_postdata(); ?>


        </div>
    </section>
    <section id="real_estate_only_best" class="real_estate_catalog only_best">
        <div class="real_estate_container">

        <!-- Controls -->
        
            <div class="real_estate_title_container ">
                  <h3 class="real_estate_title"><?php echo pll_e('Тільки найкраще'); ?></h3>
                  <div class="real_estate_tab_container">
                      <div class="real_estate_tab best all active">
                          <span class="tab_button_best all"></span>
                          <p><?php echo pll_e('Всі'); ?></p>
                      </div>
                      <div class="real_estate_tab best only">
                          <span class="tab_button_best only"></span>
                          <p><?php echo pll_e('Тільки найкраще'); ?></p>
                      </div>
                      <div class="real_estate_tab best discounts">
                          <span class="tab_button_best discounts"></span>
                          <p><?php echo pll_e('Знижки на нерухомість'); ?></p>
                      </div>
                      <div class="real_estate_tab best shares">
                          <span class="tab_button_best shares"></span>
                          <p><?php echo pll_e('Акції'); ?></p>
                      </div>
                  </div>
            </div>
            
            <div class="real_estate_result_container ">
            <?php
   $args_only_best = array(
    'post_type'      => 'product',
    'posts_per_page' => 6,
    'product_tag'=> 'shares,only,discount',
    'orderby' => 'rand',

);

$only_best = new WP_Query( $args_only_best );

while ( $only_best->have_posts() ) : $only_best->the_post();
global $product; ?>
<?php //echo wp_get_post_terms( get_the_id(), 'product_tag', array( 'fields' => 'slugs' ) ); ?>
<?php $tags = get_the_terms( get_the_ID(), 'product_tag' ); 
if( is_array( $tags ) ){
	foreach( $tags as $tag ){
        $slug_best = $tag->slug;
    }
}
?>
                  <a href="<?php echo esc_url(get_the_permalink()) ?>" class="link_wrap_product" >
                    <div class="real_estate_result_item best <?php echo $slug_best;?> active ">
                          <div class="result_head">
                              <p><?php echo get_the_title(); ?>   |   <?php echo $product->get_attribute( 'pa_floor' ); ?> <?php echo wc_attribute_label( 'pa_floor' ); ?></p>
                              <p class="size"><?php echo $product->get_attribute( 'pa_polscha' ); ?> <?php echo __( 'м²', 'new-england' ); ?></p>
                          </div>
                          <a href="<?php echo get_permalink(); ?>" class="real_estate_img_container">
                              <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="one_room_flat">
                          </a>
                          <a href="<?php echo esc_url(get_the_permalink()) ?>" class="filter_item_footer">
                          <div class="result_footer">
                              <div>
                                  <p class="flat_name"><?php echo wc_attribute_label( 'pa_building' ); ?> “<?php echo $product->get_attribute( 'pa_building' ); ?>”</p>
                                  <p class="flet_prise"><?php echo $product->get_price_html(); ?>/<?php echo __( 'м²', 'new-england' ); ?></p>
                              </div>
                              <p h class="result_item_button"><span class="arrow"></span></p>
                          </div>
</a>
                    </div>
                  </a>
                    <?php  endwhile; ?>
<?php   wp_reset_postdata(); ?>

            </div>

        </div> 
    </section>
    <section class="asking ">

    <h2 class="asking_title "><?php echo esc_attr( $lang['product_title_faq'] ); ?></h2>
    <?php
    if ($lang['product_add_faq']) :

        // Loop through rows.
        foreach ($lang['product_add_faq'] as $faq) : 
    ?>
            <div class="asking_item ">
                <div class="ask_arrow_container"><span class="ask_arrow"></span></div>
                <div class="ask_text_content">
                    <h3 class="question"><?php echo $faq['title']; ?></h3>
                    <p class="answer"><?php echo $faq['description']; ?></p>
                </div>
            </div>
    <?php
        endforeach;
    endif; ?>
    </section>
    <section class="question_form ">
        <div class="qestion_container ">
			<?php echo do_shortcode( '[cf7form cf7key="contact-form-have-any-questions"]' ); ?>
		</form>
        </div>
    </section>

</div>

<?php //do_action( 'woocommerce_after_single_product' ); ?>


<section class="popap print">
        <span class="popap_close"></span>
        <div class="print_container">
<div id="pdfhtml">
        <?php do_action('pdfContent'); ?>
</div>
            <div id="pdfhtml2"></div>
            <div class="print_link">
                <a class="print_link_item print_now">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/product_card/print.svg" alt="print">  
                    <p>Надрукувати</p>
                </a>
 

                <form id="formpdf" method="post">
                <a off-onclick="pdfDiv('pdfhtml')" id="save_pdf" class="print_link_item">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/product_card/export.svg" alt="export">
                    <p>export pdf</p>
                </a>
    <input type="hidden" name="post_id" value="<?php echo get_the_id(); ?>"/>
    <input type="hidden" name="pdfname" value="Квартира1"/>
    <input type="hidden" name="pdfcontents" value=""/>
    <input type="hidden" name="me_post_pdf_ajax" value="submitted">
</form>

                <a href="#" class="print_link_item">
                    <img class="textshareicon" src="<?php echo get_template_directory_uri(); ?>/img/product_card/share.svg" alt="share">
                    <p class="textshare">поділитися</p>
                </a>
                <?php do_action('sharepopupButtons'); ?>
            </div>
        </div>
    </section>
<?php
    //onclick="printDiv('printableArea')"
 
    ?>

    <script type="text/javascript">
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
      </script>
      