<?php

/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}
?>
<a href="<?php echo esc_url(get_the_permalink()) ?>" class="link_wrap_product" >
    <div <?php wc_product_class('about_us_flats_item swiper-slide', $product); ?>>
        <div class="flats_item_head">
            <p><?php echo $product->get_attribute('pa_kimnat'); ?>-<?php _e('кімнатна квартира', 'new_england'); ?></p>
            <p class="flats_item_head_hide"><?php echo $product->get_attribute('pa_polscha'); ?> <?php _e('м²', 'new_england'); ?></p>
        </div>
        <div class="flats_item_image">
            <img src="<?php echo wp_get_attachment_image_url($product->get_image_id(), 'full'); ?>" alt="Photo">
        </div>
        <a href="<?php echo esc_url(get_the_permalink()) ?>" class="flats_item_footer">
            <div>
                <p class="flats_item_name"><?php _e('Будинок', 'new_england');
                                            echo ' “' . $product->get_attribute('pa_building'); ?>”</p>
                <p class="flats_item_price"><?php echo $product->get_price_html() ?>/м²</p>
            </div>
            <p class="result_item_button"><span class="arrow"></span></p>
        </a>
    </div>
</a>
<!-- <li <?php wc_product_class('filter_result_item', $product); ?>>
	<?php
    /**
     * Hook: woocommerce_before_shop_loop_item.
     *
     * @hooked woocommerce_template_loop_product_link_open - 10
     */
    do_action('woocommerce_before_shop_loop_item');

    /**
     * Hook: woocommerce_before_shop_loop_item_title.
     *
     * @hooked woocommerce_show_product_loop_sale_flash - 10
     * @hooked woocommerce_template_loop_product_thumbnail - 10
     */
    do_action('woocommerce_before_shop_loop_item_title');

    /**
     * Hook: woocommerce_shop_loop_item_title.
     *
     * @hooked woocommerce_template_loop_product_title - 10
     */
    do_action('woocommerce_shop_loop_item_title');

    /**
     * Hook: woocommerce_after_shop_loop_item_title.
     *
     * @hooked woocommerce_template_loop_rating - 5
     * @hooked woocommerce_template_loop_price - 10
     */
    do_action('woocommerce_after_shop_loop_item_title');

    /**
     * Hook: woocommerce_after_shop_loop_item.
     *
     * @hooked woocommerce_template_loop_product_link_close - 5
     * @hooked woocommerce_template_loop_add_to_cart - 10
     */
    do_action('woocommerce_after_shop_loop_item');
    ?>
</li> -->