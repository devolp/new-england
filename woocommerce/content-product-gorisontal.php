<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product-gorisontal.php.
 *
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<div class="result_list_item">
    <div class="result_list_photo_container">
        <img src="<?php echo wp_get_attachment_image_url( $product->get_image_id(), 'full' ); ?>" alt="Result_photo">
    </div>
    <div class="result_list_text_container">
        <div class="result_list_text_head">
            <p class="number_of_room"><?php echo $product->get_title(); ?><?php //echo $product->get_attribute( 'pa_kimnat' ); ?></p>
            <p class="size"><?php echo $product->get_attribute( 'pa_polscha' ); ?> м²</p>
        </div>
        <div class="result_list_text_footer">
            <div class="list_footer_text">
                <p class="house_name"><?php echo pll_e('Будинок') . ' “' . $product->get_attribute( 'pa_building' ); ?>”</p>
                <p class="hous_prise"><?php echo $product->get_price_html() ?>/м²</p>
            </div>
            <a href="<?php echo esc_url(get_the_permalink()) ?>" class="result_item_button"><span class="arrow"></span></a>
        </div>
    </div>
</div>
