<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

get_header('shop');

$pllang = pll_current_language();
$lang = get_field($pllang, 'option');

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */

do_action('woocommerce_before_main_content');

?>

<section class="main_screen_catalog">

	<?php if (is_shop()) { ?>
		<?php $header = get_field('header', wc_get_page_id('shop')); ?>
		<div class="catalog_container">
			<div class="catalog_item">
				<div class="catalog_image_container">
					<img src="<?php echo $header['category_1']['image'] ?>" alt="<?php echo $header['category_1']['header'] ?>">
				</div>
				<h2 class="catalog_item_title"><?php echo $header['category_1']['header'] ?></h2>
				<p class="catalog_item_text"><?php echo $header['category_1']['description'] ?></p>
				<a href="<?php echo $header['category_1']['link'] ?>" class="result_item_button"><span class="arrow"></span></a>
			</div>
			<div class="catalog_item">
				<div class="catalog_image_container">
					<img src="<?php echo $header['category_2']['image'] ?>" alt="<?php echo $header['category_2']['header'] ?>">
				</div>
				<h2 class="catalog_item_title"><?php echo $header['category_2']['header'] ?></h2>
				<p class="catalog_item_text"><?php echo $header['category_2']['description'] ?></p>
				<a href="<?php echo $header['category_2']['link'] ?>" class="result_item_button"><span class="arrow"></span></a>
			</div>
		</div>
	<?php } else { ?>
		<div class="catalog_container">
			<div class="catalog_header-col">
				<?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
					<h2 class="catalog_title"><?php woocommerce_page_title(); ?></h2>
				<?php endif; ?>
			</div>

			<div class="catalog_header-col catalog_header_text">
				<?php if (!is_search()) {
					if (is_post_type_archive('product') && in_array(absint(get_query_var('paged')), array(0, 1), true)) {
						$shop_page = get_post(wc_get_page_id('shop'));
						if ($shop_page) {
							$description = wc_format_content(wp_kses_post($shop_page->post_content));
							if ($description) {
								echo '<div class="addReadMore showlesscontent">';
								echo wp_strip_all_tags($description);
								echo '</div>';
							}
						}
					} elseif (is_product_taxonomy() && 0 === absint(get_query_var('paged'))) {
						$term = get_queried_object();

						if ($term && !empty($term->description)) {
							//echo wc_format_content(wp_kses_post($term->description));
                            echo '<div class="addReadMore showlesscontent">';
							    echo wp_strip_all_tags($term->description);
							echo '</div>';
						}
					}
				} ?>
			</div>
        </div>
	<?php } ?>
	<!-- <header class="woocommerce-products-header">
		<?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
			<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
		<?php endif; ?>

		<?php
		/**
		 * Hook: woocommerce_archive_description.
		 *
		 * @hooked woocommerce_taxonomy_archive_description - 10
		 * @hooked woocommerce_product_archive_description - 10
		 */
		do_action('woocommerce_archive_description');
		?>
	</header> -->

	<div class="filter _anim_items  _anim-no-hide _active">
		<div class="filter_head _amin-show">
			<div class="filter_tab_button_container">
				<div id="filterTableBtn" class="filter_tab_button first">
					<span class="first"></span>
					<span class="two"></span>
					<span class="three"></span>
					<span class="four"></span>
					<span class="last"></span>
				</div>
				<div class="filter_tab_button second">
					<span class="first"></span>
					<span class="two"></span>
					<span class="three"></span>
					<span class="four"></span>
					<span class="five"></span>
					<span class="last"></span>
				</div>
				<div class="filter_tab_button last active">
					<span class="first"></span>
					<span class="two"></span>
					<span class="three"></span>
					<span class="last"></span>
				</div>
			</div>
			<?php woocommerce_catalog_ordering(); ?>
		</div>
		<div class="filter_mobile_and_table_head _amin-show">
			<div class="filter_table_head">
				<a class="filter_table_button" href="#"><?php echo pll_e('Фільтр') ?></a>
				<a class="filter_table_button_show_col" href="#"><?php echo pll_e('Поля таблицы') ?></a>
				<form action="#" method="POST">
					<label for="table_search"></label>
					<input type="search" name="search" id="table_search">
				</form>
			</div>
		</div>
		<div class="filter_selected_item_container">
			<?php if (is_active_sidebar('active_filters')) : ?>
				<?php dynamic_sidebar('active_filters'); ?>
			<?php endif; ?>
		</div>
		<div class="filter_item_container first">
			<?php if (is_active_sidebar('filters')) : ?>
				<?php dynamic_sidebar('filters'); ?>
			<?php endif; ?>
		</div>
		<div class="filter_item_container_show_col">
			<div class="filter_item_container_show_col_wrap">
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_numsect" class="filter-show-btn" data-cookies="filterShowButton_numsect" data-table_col=".nummer_section_result" data-filter-item="#numberSection">
					<label for="filterShowButton_numsect"><?php echo pll_e('№ секції') ?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_floor" class="filter-show-btn" data-cookies="filterShowButton_floor" data-table_col=".flor_result_fulls" data-filter-item="#floor">
					<label for="filterShowButton_floor"><?php echo pll_e('Поверх') ?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_numroom" class="filter-show-btn" data-cookies="filterShowButton_numroom" data-table_col=".rum_result_fulls" data-filter-item="#number_of_room">
					<label for="filterShowButton_numroom"><?php echo pll_e('К-сть кімнат')?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_area" class="filter-show-btn" data-cookies="filterShowButton_area" data-table_col=".area_m2_section_result_firsts" data-filter-item="#area_m2">
					<label for="filterShowButton_area"><?php echo pll_e('Площа м²')?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_house" class="filter-show-btn" data-cookies="filterShowButton_house" data-table_col=".house_result" data-filter-item="#buildingSelect">
					<label for="filterShowButton_house"><?php echo pll_e('Дім')?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_numflet" class="filter-show-btn" data-cookies="filterShowButton_numflet" data-table_col=".nummer_flet_result_fulls" data-filter-item="#nummer_flet">
					<label for="filterShowButton_numflet"><?php echo pll_e('№ квартири') ?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_type" class="filter-show-btn" data-cookies="filterShowButton_type" data-table_col=".real_estate_section_result" data-filter-item="#typeSelect">
					<label for="filterShowButton_type"><?php echo pll_e('Тип нерухомості') ?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_total" class="filter-show-btn" data-cookies="filterShowButton_total" data-table_col=".total_result_firsts" data-filter-item="#total">
					<label for="filterShowButton_total"><?php echo pll_e('Загальна сума') ?></label>
				</div>
				<div class="filter_table_show_col_group_input">
					<input type="checkbox" name="" checked="checked" id="filterShowButton_status" class="filter-show-btn" data-cookies="filterShowButton_status" data-table_col=".status_result" data-filter-item="#statusSelect">
					<label for="filterShowButton_status"><?php echo pll_e('Статус') ?></label>
				</div>	
			</div>
		</div>
		<a href="#" class="collapse_filter _amin-show"><?php echo pll_e('розгорнути фільтр') ?></a>
	</div>

	<?php
	if (woocommerce_product_loop()) {

		/**
		 * Hook: woocommerce_before_shop_loop.
		 *
		 * @hooked woocommerce_output_all_notices - 10
		 * @hooked woocommerce_result_count - 20
		 * @hooked woocommerce_catalog_ordering - 30
		 */
		// do_action( 'woocommerce_before_shop_loop' );
	?>

		<div class="filter_result_container_main _anim_items  _anim-no-hide _active">			
			<div class="block active tab_container _amin-show">
				<div class="filter_result_container block">

					<?php
					woocommerce_product_loop_start();
					if (wc_get_loop_prop('total')) {
						while (have_posts()) {
							the_post();
							/**
							 * Hook: woocommerce_shop_loop.
							 */
							do_action('woocommerce_shop_loop');

							wc_get_template_part('content', 'product');
						}
					}
					woocommerce_product_loop_end();
					?>
				</div>
			</div>

			<div class="list tab_container">
				<div class="filter_result_container_list">

					<?php
					woocommerce_product_loop_start();
					if (wc_get_loop_prop('total')) {
						while (have_posts()) {
							the_post();
							/**
							 * Hook: woocommerce_shop_loop.
							 */
							do_action('woocommerce_shop_loop');

							wc_get_template_part('content', 'product-gorisontal');
						}
					}
					woocommerce_product_loop_end();
					?>

				</div>
			</div>

			<div class="filter_result_container_table tab_container">
				<form action="#" method="POST" class="table_options_container desctop">
					<div class="filter_item__wrapper nummer_section_counter table_filter-sorting">
						<label for="nummer" title="<?php echo pll_e('№ секції') ?>" ><?php echo pll_e('№ секції') ?></label>
						<input id="numberSection" class="table_filter_item" data-table_col=".nummer_section_result" name="nummer_section">
						<div class="array_block" data-column="nummer_section_result">	
							<span class="arrow_prew"></span>
							<span class="arrow_next"></span>
						</div>
					</div>
					<div class="filter_item__wrapper floor_section_counter table_filter-sorting">
						<label for="floor" title="<?php echo pll_e('Поверх') ?>"><?php echo pll_e('Поверх')?></label>
						<input type="number" id="floor" class="table_filter_item" data-table_col=".flor_result_fulls" name="floor_section">
						<div class="array_block" data-column="flor_result_fulls">	
							<span class="arrow_prew"></span>
							<span class="arrow_next"></span>
						</div>
					</div>
					<div class="filter_item__wrapper number_of_room_section_counter table_filter-sorting">
						<label for="number_of_room" title="<?php echo pll_e('К-сть кімнат')?>"><?php echo pll_e('К-сть кімнат')?></label>
						<input type="number" id="number_of_room" class="table_filter_item" data-table_col=".rum_result_fulls" name="number_of_room_section">
						<div class="array_block" data-column="rum_result_fulls">	
							<span class="arrow_prew"></span>
							<span class="arrow_next"></span>
						</div>
					</div>
					<div class="filter_item__wrapper area_m2_section_counter table_filter-sorting">
						<label for="area_m2" title="<?php echo pll_e('Площа м²')?>"><?php echo pll_e('Площа м²')?></label>
						<input id="area_m2" class="table_filter_item" data-table_col=".area_m2_section_result_firsts" name="area_m2_section">
						<div class="array_block" data-column="area_m2_section_result_firsts">	
							<span class="arrow_prew"></span>
							<span class="arrow_next"></span>
						</div>
					</div>
					<div class="filter_item__wrapper house_select">
						<p title="<?php echo pll_e('Дім')?>"><?php echo pll_e('Дім')?></p>
						<select id="buildingSelect" class="table_filter_item table_filter_item_select" data-table_col=".house_result">
						</select>
					</div>
					<div class="filter_item__wrapper nummer_flet_section_counter table_filter-sorting">
						<label for="nummer_flet" title="<?php echo pll_e('№ квартири') ?>"><?php echo pll_e('№ квартири') ?></label>
						<input id="nummer_flet" class="table_filter_item" data-table_col=".nummer_flet_result_fulls" name="nummer_flet_section">
						<div class="array_block" data-column="nummer_flet_result_fulls">	
							<span class="arrow_prew"></span>
							<span class="arrow_next"></span>
						</div>
					</div>
					<div class="filter_item__wrapper real_estate_select">
						<p title="<?php echo pll_e('Тип нерухомості') ?>"><?php echo pll_e('Тип нерухомості') ?></p>
						<select id="typeSelect" class="table_filter_item table_filter_item_select" data-table_col=".real_estate_section_result">
						</select>
					</div>
					<div class="filter_item__wrapper total_section_counter table_filter-sorting">
						<label for="total" title="<?php echo pll_e('Загальна сума') ?>"><?php echo pll_e('Загальна сума') ?></label>
						<input type="number" id="total" class="table_filter_item" data-table_col=".total_result_firsts" name="total_flet_section">
						<div class="array_block" data-column="total_result_firsts">	
							<span class="arrow_prew"></span>
							<span class="arrow_next"></span>
						</div>
					</div>
					<div class="filter_item__wrapper status_select">
						<p title="<?php echo pll_e('Статус') ?>"><?php echo pll_e('Статус') ?></p>
						<select id="statusSelect" class="table_filter_item table_filter_item_select" data-table_col=".status_result">
						</select>
					</div>
				</form>
				<div id="filter_table_result_desctop" class="filter_table_result desctop filter_table_result_desctop"></div>
			</div>
			<?php 
				global $wp_query;
				if ($wp_query->max_num_pages > 1) : ?>
				<script>
					var ajaxurl = '<?php echo site_url(); ?>/wp-admin/admin-ajax.php';
					var posts_vars = '<?php echo serialize($wp_query->query_vars); ?>';
					var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
					var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
				</script>
				<a href="#" class="show_more _amin-show" id="loadmore_products">
					<?php echo $lang['button_text']; ?></a>
			<?php endif; ?>
			<script type="text/javascript">
				var load = '<?= $lang['button_load']; ?>';
				var loadmore = '<?= $lang['button_text']; ?>';
				var ajaxurl = '<?php echo site_url(); ?>/wp-admin/admin-ajax.php';
				var posts_vars = '<?php echo serialize($wp_query->query_vars); ?>';
				var all_option = '<?php echo pll_e('Всі'); ?>';
			</script>
		</div>
		<?php
			} else {
				/**
				 * Hook: woocommerce_no_products_found.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action('woocommerce_no_products_found');
			}

		?>
</section>

<section id="real_estate_only_best" class="real_estate_catalog only_best">

	<div class="real_estate_container">

		<div class="real_estate_title_container _anim_items _anim-no-hide">
			<h3 class="real_estate_title"><?php echo pll_e('Тільки найкраще'); ?></h3>
			<div class="real_estate_tab_container">
				<div class="real_estate_tab best all active">
					<span class="tab_button_best all"></span>
					<p><?php echo pll_e('Всі'); ?></p>
				</div>
				<div class="real_estate_tab best only">
					<span class="tab_button_best only"></span>
					<p><?php echo pll_e('Тільки найкраще'); ?></p>
				</div>
				<div class="real_estate_tab best discounts">
					<span class="tab_button_best discounts"></span>
					<p><?php echo pll_e('Знижки на нерухомість'); ?></p>
				</div>
				<div class="real_estate_tab best shares">
					<span class="tab_button_best shares"></span>
					<p><?php echo pll_e('Акції'); ?></p>
				</div>
			</div>
		</div>

		<div class="real_estate_result_container _anim_items _anim-no-hide">
			<?php
			$args_only_best = array(
				'post_type'      => 'product',
				'posts_per_page' => 6,
				'product_tag' => 'shares,only,discount',
				'orderby' => 'rand',

			);

			$only_best = new WP_Query($args_only_best);

			while ($only_best->have_posts()) : $only_best->the_post();
				global $product; ?>
				<?php //echo wp_get_post_terms( get_the_id(), 'product_tag', array( 'fields' => 'slugs' ) ); 
				?>
				<?php $tags = get_the_terms(get_the_ID(), 'product_tag');
				if (is_array($tags)) {
					foreach ($tags as $tag) {
						$slug_best = $tag->slug;
					}
				}
				?>
			<a href="<?php echo esc_url(get_the_permalink()) ?>" class="link_wrap_product" >
				<div class="real_estate_result_item best <?php echo $slug_best; ?> active _amin-show">
					<div class="result_head">
						<p><?php echo get_the_title(); ?> | <?php echo $product->get_attribute('pa_floor'); ?> <?php echo wc_attribute_label('pa_floor'); ?></p>
						<p class="size"><?php echo $product->get_attribute('pa_polscha'); ?> <?php echo __('м²', 'new-england'); ?></p>
					</div>
					<a href="<?php echo get_permalink(); ?>" class="real_estate_img_container">
						<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="one_room_flat">
					</a>
					<a href="<?php echo esc_url(get_the_permalink()) ?>" class="result_footer">
						<div>
							<p class="flat_name"><?php echo wc_attribute_label('pa_building'); ?> “<?php echo $product->get_attribute('pa_building'); ?>”</p>
							<p class="flet_prise"><?php echo $product->get_price_html(); ?>/<?php echo __('м²', 'new-england'); ?></p>
						</div>
						<p  class="result_item_button"><span class="arrow"></span></p>
					</a>
				</div>
			</a>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>

		</div>

	</div>
</section>

<section class="asking _anim_items _anim-no-hide _active">

	<h2 class="asking_title _anim_items _anim-no-hide"><?php echo esc_attr($lang['title_faq']); ?></h2>
	<?php
		if ($lang['add_faq']) :

			// Loop through rows.
			foreach ($lang['add_faq'] as $faq) :
	?>
			<div class="asking_item _amin-show">
				<div class="ask_arrow_container"><span class="ask_arrow"></span></div>
				<div class="ask_text_content">
					<h3 class="question"><?php echo $faq['title']; ?></h3>
					<p class="answer"><?php echo $faq['description']; ?></p>
				</div>
			</div>
	<?php
			endforeach;
		endif; ?>
</section>

<section class="question_form _anim_items _anim-no-hide _active">
	<div class="qestion_container _amin-show">
			<?php echo do_shortcode('[cf7form cf7key="contact-form-have-any-questions"]'); ?>
		</form>
	</div>
</section>

<?php 
get_footer();
