<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<!-- breadcrumb -->
<section class="crumbs">
	<div class="crumbs_content">
		<div class="bread_crumbs_container">
			
			<div class="bread_crumbs_content">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					} 
				?>
			</div>

			<?php //do_action('shareButtons'); ?>

			
<?php if ( is_product() ) { ?>
			<a href="#" class="link_print_container">
				<svg class="link_print" width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M5 8V1H17V8" stroke="#1C1F1E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M5 17H3C2.46957 17 1.96086 16.7893 1.58579 16.4142C1.21071 16.0391 1 15.5304 1 15V10C1 9.46957 1.21071 8.96086 1.58579 8.58579C1.96086 8.21071 2.46957 8 3 8H19C19.5304 8 20.0391 8.21071 20.4142 8.58579C20.7893 8.96086 21 9.46957 21 10V15C21 15.5304 20.7893 16.0391 20.4142 16.4142C20.0391 16.7893 19.5304 17 19 17H17" stroke="#1C1F1E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M17 13H5V21H17V13Z" stroke="#1C1F1E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>                        
			</a>
<span class="share_link_item_span">
                <a href="#" class="share_link_item">
                    <img width="22" height="22" class="textshareicon" src="<?php echo get_template_directory_uri(); ?>/img/product_card/share.svg" alt="share">

                </a>
                <?php do_action('sharepopupButtons'); ?>
</span>
<?php } else { ?>
    <span class="share_link_item_span">
                <a href="#" class="share_link_item">
                    <img width="22" height="22" class="textshareicon" src="<?php echo get_template_directory_uri(); ?>/img/product_card/share.svg" alt="share">

                </a>
                <?php do_action('sharepopupButtons'); ?>
</span>
            <?php } ?>
		</div>
	</div>

</section>
<!-- end breadcrumb -->
