<?php

/**
 * Template Name: Шаблон контент сторінки
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package New_England
 */

get_header();

get_template_part('woocommerce/global/breadcrumb');
?>

<section class="blog-content">
    <div class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="blog-content-top">
                    <div class="blog-content-left">

                        <h1><?php the_title(); ?></h1>
                        <?php $parts = explode('</p>', get_the_content());
                        $content_top = $parts[0] . '</p>' . $parts[1]  . '</p>';
                        echo $content_top;
                        ?>
                    </div>
                    <div class="blog-content-right">
                        <div class="blog-content-right-image">

                            <?php $image1 = get_field('image_1');
                            if ($image1) : ?>
                                <img src="<?php echo esc_url($image1['url']); ?>" alt="<?php echo esc_attr($image1['alt']); ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="blog-content-mid">
                    <div class="blog-content-left">
                        <div class="blog-content-left-image">
                            <?php $image2 = get_field('image_2');
                            if ($image2) : ?>
                                <img src="<?php echo esc_url($image2['url']); ?>" alt="<?php echo esc_attr($image2['alt']); ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="blog-content-right">
                        <?php
                        unset($parts[0]);
                        unset($parts[1]);
                        $content_bottom = implode('</p>', $parts);
                        echo $content_bottom; ?>
                    </div>
                </div>
        <?php endwhile;
        endif; ?>

        <?php
        $images = get_field('gallery');
        if ($images) : ?>
            <div class="blog_content_bottom">
                <?php foreach ($images as $image) : ?>
                    <div class="blog_bottom_image">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>

<?php
get_template_part('template-parts/have-any-questions-section');

get_footer(); ?>