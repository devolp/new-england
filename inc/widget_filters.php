<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
use  Premmerce\Filter\Widget\FilterWidget ;
use  Premmerce\Filter\Shortcodes\FilterWidgetShortcodes ;
$dropdownList = [ 'dropdown', 'scroll_dropdown', 'dropdown_hover' ];
$scrollList = [ 'scroll', 'scroll_dropdown' ];
?>

<?php 
echo  ( !empty($args['before_widget']) ? $args['before_widget'] : '' ) ;
?>
<div class="popap_container">
    <h2 class="popap_title"><?php _e('Пошук нерухомості', 'new_england'); ?></h2>
    <form id="jsSearchForm" action="<?php echo get_the_permalink(pll_get_post(wc_get_page_id('shop'))); ?>" method="GET" name="form">
        <?php         
        foreach ( $attributes as $key => $attribute ) {
            if (($attribute->attribute_name == "price") 
                || ($attribute->attribute_name == "building") 
                || ($attribute->attribute_name == "type-neruhomosti") 
                || ($attribute->attribute_name == "poverhovist")
                || ($attribute->attribute_name == "kimnat")
                ) {
                    if ($attribute->attribute_name == "price") {
                        do_action( 'premmerce_filter_popup_render_item_slider', $attribute ); 
                    } else {
                        do_action( 'premmerce_filter_popup_render_item_select', $attribute ); 
                    }
            }
        } ?>
        <input type="submit" id="jsSearchPopupSubmitBtn" class="popap_submit" value="пошук">
    </form>
</div>
</div>
<?php 
return true;
?>

