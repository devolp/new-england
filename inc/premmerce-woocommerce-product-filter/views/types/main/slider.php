<?php if ( ! defined('ABSPATH')) {
	exit;
}

$values = $attribute->values;
$pllang = pll_current_language();

global $WOOCS;
$currencies = $WOOCS->get_currencies();
$msrp = 1 * $currencies[$WOOCS->current_currency]['rate'];
$msrp = number_format($msrp, 2, $WOOCS->decimal_sep, '');
?>

<div class="filter__inner" data-premmerce-slider-scope>
    <form action="<?php echo apply_filters('premmerce_product_filter_form_action', '') ?>" method="GET"
          class="filter__slider-form" data-premmerce-filter-slider-form>
        <div class="filter__slider-control-group">
            <span class="filter__slider_from-to filter__slider-from-span"><?php
	            if($pllang == 'uk'){
		            echo 'Від'; }
                elseif($pllang == 'ru') {
		            echo 'От';
	            } elseif($pllang == 'en') {
		            echo 'From';
	            } ?>
            </span>
            <div class="filter__slider-control-column">
                <input class="filter__slider-control from_price_in_dollar" type="number" autocomplete="off"
                       data-premmerce-filter-slider-min="<?php echo $values['min'] ?>"
                       name="min_<?php echo $attribute->attribute_name; ?>" value="<?php echo $values['min_selected'] ?>"
                       data-premmerce-slider-trigger-change>
				<?php if($msrp > 1) { ?>
                    <input class="filter__slider-control from_price_in_hrivna" type="number" autocomplete="off"
                           data-premmerce-filter-slider-min="<?php echo $values['min'] ?>"
                           off-name="min_price" value="<?php echo $WOOCS->woocs_exchange_value($values['min_selected']) ?>"
                           data-premmerce-slider-trigger-change>
				<?php } ?>
            </div>
            <span class="filter__slider_from-to filter__slider-to-span"><?php
				if($pllang == 'uk'){
					echo 'До'; }
                elseif($pllang == 'ru') {
					echo 'до';
				} elseif($pllang == 'en') {
					echo 'To';
				} ?></span>
            <div class="filter__slider-control-column">
                <input class="filter__slider-control to_price_in_dollar" type="number" autocomplete="off"
                       data-premmerce-filter-slider-max="<?php echo $values['max'] ?>"
                       name="max_<?php echo $attribute->attribute_name; ?>" value="<?php echo $values['max_selected'] ?>"
                       data-premmerce-slider-trigger-change>
				<?php if($msrp > 1) { ?>
                    <input class="filter__slider-control to_price_in_hrivna" type="number" autocomplete="off"
                           data-premmerce-filter-slider-max="<?php echo $values['max'] ?>"
                           off-name="max_<?php echo $attribute->attribute_name; ?>" value="<?php echo $WOOCS->woocs_exchange_value($values['max_selected']) ?>"
                           data-premmerce-slider-trigger-change>
				<?php } ?>
            </div>
            <span class="filter__slider_from-to filter__sliderafter">
                <?php if($msrp > 1) { ?>грн.<?php } else { ?>$<?php } ?></span>
        </div>
        <div class="filter__range-slider">
            <div class="pc-range-slider">
                <div class="pc-range-slider__wrapper">
                    <div class="pc-range-slider__control" data-premmerce-filter-range-slider></div>
                </div>
            </div>
        </div>
		<?php if($msrp > 1) { ?>
            <script>
                jQuery('.to_price_in_hrivna').on('ready change load', function (){
                    var currency_val = parseFloat(jQuery('#current_currency').data('currency'));
                    var in_hrivna = jQuery(this).val();
                    var total = (in_hrivna / currency_val).toFixed(0);
                    jQuery('.to_price_in_dollar').val(total);
                });
                jQuery('.from_price_in_hrivna').on('ready change load', function (){
                    var currency_val = parseFloat(jQuery('#current_currency').data('currency'));
                    var in_hrivna = jQuery(this).val();
                    var total = (in_hrivna / currency_val).toFixed(0);
                    jQuery('.from_price_in_dollar').val(total);
                });
                jQuery('.from_price_in_dollar, .to_price_in_dollar').hide();
            </script>
		<?php } ?>
		<?php wc_query_string_form_fields(apply_filters('premmerce_product_filter_slider_include_fields',
			$_GET,
			['max_' . $attribute->attribute_name, 'min_' . $attribute->attribute_name])) ?>
    </form>
</div>
