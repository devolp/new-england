<?php if ( ! defined('ABSPATH')) {
    exit;
}
?>
<select class="popup_filter__select" name="<?= 'filter_' . $attribute->attribute_name ?>" data-attribute-name="<?php echo $attribute->attribute_name ?>">
    <option value=""><?php printf(__('%s',
            'woocommerce'),
            $attribute->attribute_label) ?></option>
    <?php foreach ($attribute->terms as $term): ?>
        <?php $selected = $term->checked ? 'selected' : ''; ?>
        <option <?php echo $selected ?> value="<?php echo $term->slug ?>">
            <?php echo $term->name . ' (' . ($term->count) . ')' ?>
        </option>
    <?php endforeach ?>
</select>
