<?php if ( ! defined('ABSPATH')) {
    exit;
}

$values = $attribute->values;
?>
<div class="size_popap_container">
    <span class="popap_search_input_title"><?php _e('Ціна за м2', 'woocommerce') ?></span>
    <div class="filter__inner" data-premmerce-slider-scope>
        <div class="filter__slider-control-group popap_input_container">
            <span><?php echo pll_e('Від') ?></span>
            <div class="filter__slider-control-column">
                <input class="filter__slider-control" type="number" autocomplete="off"
                    data-premmerce-filter-slider-min="<?php echo $values['min'] ?>"
                    name="min_<?php echo $attribute->attribute_name; ?>" value="<?php /* echo $values['min_selected'] */ ?>" placeholder="<?php echo $values['min_selected'] ?>"
                    data-premmerce-slider-trigger-change>
            </div>
            <span><?php echo pll_e('До') ?></span>
            <div class="filter__slider-control-column">
                <input class="filter__slider-control" type="number" autocomplete="off"
                    data-premmerce-filter-slider-max="<?php echo $values['max'] ?>"
                    name="max_<?php echo $attribute->attribute_name; ?>" value="<?php /* echo $values['max_selected'] */ ?>" placeholder="<?php echo $values['max_selected'] ?>"
                    data-premmerce-slider-trigger-change>
            </div>
        </div>
    </div>
</div>